package be.simplenotes.app.filters

import be.simplenotes.app.filters.auth.OptionalAuthFilter
import be.simplenotes.app.filters.auth.OptionalAuthLens
import be.simplenotes.app.filters.auth.RequiredAuthFilter
import be.simplenotes.app.filters.auth.RequiredAuthLens
import be.simplenotes.config.JwtConfig
import be.simplenotes.domain.security.SimpleJwt
import be.simplenotes.domain.security.UserJwtMapper
import be.simplenotes.types.LoggedInUser
import com.natpryce.hamkrest.assertion.assertThat
import io.micronaut.context.BeanContext
import io.micronaut.inject.qualifiers.Qualifiers
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.RequestContexts
import org.http4k.core.Response
import org.http4k.core.Status.Companion.FOUND
import org.http4k.core.Status.Companion.OK
import org.http4k.core.cookie.cookie
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.hamkrest.hasBody
import org.http4k.hamkrest.hasHeader
import org.http4k.hamkrest.hasStatus
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

internal class RequiredAuthFilterTest {

    // region setup
    private val jwtConfig = JwtConfig("secret", 1, TimeUnit.HOURS)
    private val simpleJwt = SimpleJwt(jwtConfig, UserJwtMapper())

    private val beanCtx = BeanContext.build()
        .registerSingleton(jwtConfig)
        .start()

    private inline fun <reified T> BeanContext.getBean(): T = getBean(T::class.java)
    private inline fun <reified T> BeanContext.getBean(name: String): T =
        getBean(T::class.java, Qualifiers.byName(name))

    private val requiredAuth = beanCtx.getBean<RequiredAuthFilter>()
    private val requiredLens = beanCtx.getBean<RequiredAuthLens>("required")

    private val optionalAuth = beanCtx.getBean<OptionalAuthFilter>()
    private val optionalLens = beanCtx.getBean<OptionalAuthLens>("optional")

    private val ctx = beanCtx.getBean<RequestContexts>()

    private val app = ServerFilters.InitialiseRequestContext(ctx).then(
        routes(
            "/optional" bind GET to optionalAuth.then { request: Request ->
                Response(OK).body(optionalLens(request).toString())
            },
            "/protected" bind GET to requiredAuth.then { request: Request ->
                Response(OK).body(requiredLens(request).toString())
            }
        )
    )
    // endregion

    @Nested
    inner class OptionalAuth {
        @Test
        fun `it should allow no token`() {
            val response = app(Request(GET, "/optional"))
            assertThat(response, hasStatus(OK))
            assertThat(response, hasBody("null"))
        }

        @Test
        fun `it should allow an invalid token`() {
            val response = app(Request(GET, "/optional").cookie("Bearer", "nnkjnkjnk"))
            assertThat(response, hasStatus(OK))
            assertThat(response, hasBody("null"))
        }

        @Test
        fun `it should allow a valid token`() {
            val jwtPayload = LoggedInUser(1, "user")
            val token = simpleJwt.sign(jwtPayload)
            val response = app(Request(GET, "/optional").cookie("Bearer", token))
            assertThat(response, hasStatus(OK))
            assertThat(response, hasBody("$jwtPayload"))
        }
    }

    @Nested
    inner class RequiredAuth {
        @Test
        fun `it shouldn't allow a missing token`() {
            val response = app(Request(GET, "/protected"))
            assertThat(response, hasStatus(FOUND))
            assertThat(response, hasHeader("Location"))
        }

        @Test
        fun `it shouldn't allow an invalid token`() {
            val response = app(Request(GET, "/protected").cookie("Bearer", "nnkjnkjnk"))
            assertThat(response, hasStatus(FOUND))
            assertThat(response, hasHeader("Location"))
        }

        @Test
        fun `it should allow a valid token`() {
            val jwtPayload = LoggedInUser(1, "user")
            val token = simpleJwt.sign(jwtPayload)
            val response = app(Request(GET, "/protected").cookie("Bearer", token))
            assertThat(response, hasStatus(OK))
            assertThat(response, hasBody("$jwtPayload"))
        }
    }
}
