import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.kotlinx-serialization")
    id("be.simplenotes.app-shadow")
    id("be.simplenotes.docker")
    id("be.simplenotes.micronaut")
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":views"))
    implementation(project(":css"))

    implementation(libs.http4k.core)
    implementation(libs.bundles.jetty)
    implementation(libs.kotlinx.serialization.json)

    implementation(libs.slf4j.api)
    runtimeOnly(libs.slf4j.logback)

    micronaut()

    testImplementation(libs.bundles.test)
    testImplementation(libs.http4k.testing.hamkrest)
}

docker {
    image = "hubv/simplenotes"
    tag = "latest"
}
