package be.simplenotes.app.controllers

import be.simplenotes.app.extensions.html
import be.simplenotes.types.LoggedInUser
import be.simplenotes.views.BaseView
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import javax.inject.Singleton

@Singleton
class BaseController(private val view: BaseView) {
    fun index(@Suppress("UNUSED_PARAMETER") request: Request, loggedInUser: LoggedInUser?) =
        Response(OK).html(view.renderHome(loggedInUser))
}
