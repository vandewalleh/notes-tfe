package be.simplenotes.app.controllers

import be.simplenotes.app.extensions.html
import be.simplenotes.app.extensions.redirect
import be.simplenotes.domain.DeleteError
import be.simplenotes.domain.DeleteForm
import be.simplenotes.domain.ExportService
import be.simplenotes.domain.UserService
import be.simplenotes.types.LoggedInUser
import be.simplenotes.views.SettingView
import org.http4k.core.*
import org.http4k.core.body.form
import org.http4k.core.cookie.invalidateCookie
import javax.inject.Singleton

@Singleton
class SettingsController(
    private val userService: UserService,
    private val exportService: ExportService,
    private val settingView: SettingView,
) {
    fun settings(request: Request, loggedInUser: LoggedInUser): Response {
        if (request.method == Method.GET)
            return Response(Status.OK).html(settingView.settings(loggedInUser))

        val deleteForm = request.deleteForm(loggedInUser)
        val result = userService.delete(deleteForm)

        return result.fold(
            {
                when (it) {
                    DeleteError.Unregistered -> Response.redirect("/").invalidateCookie("Bearer")
                    DeleteError.WrongPassword -> Response(Status.OK).html(
                        settingView.settings(
                            loggedInUser,
                            error = "Wrong password"
                        )
                    )
                    is DeleteError.InvalidForm -> Response(Status.OK).html(
                        settingView.settings(
                            loggedInUser,
                            validationErrors = it.validationErrors
                        )
                    )
                }
            },
            {
                Response.redirect("/").invalidateCookie("Bearer")
            }
        )
    }

    private fun attachment(filename: String, contentType: String) = { response: Response ->
        val name = filename.replace("[^a-zA-Z0-9-_.]".toRegex(), "_")
        response
            .header("Content-Disposition", "attachment; filename=\"$name\"")
            .header("Content-Type", contentType)
    }

    fun export(request: Request, loggedInUser: LoggedInUser): Response {
        val isDownload = request.form("download") != null

        return if (isDownload) {
            val filename = "simplenotes-export-${loggedInUser.username}"
            if (request.form("format") == "zip") {
                val zip = exportService.exportAsZip(loggedInUser.userId)
                Response(Status.OK)
                    .with(attachment("$filename.zip", "application/zip"))
                    .body(zip)
            } else
                Response(Status.OK)
                    .with(attachment("$filename.json", "application/json"))
                    .body(exportService.exportAsJson(loggedInUser.userId))
        } else Response(Status.OK).body(exportService.exportAsJson(loggedInUser.userId)).header(
            "Content-Type",
            "application/json"
        )
    }

    private fun Request.deleteForm(loggedInUser: LoggedInUser) =
        DeleteForm(loggedInUser.username, form("password"), form("checked") != null)
}
