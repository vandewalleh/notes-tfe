package be.simplenotes.app.utils

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import javax.inject.Singleton

interface StaticFileResolver {
    fun resolve(name: String): String?
}

@Singleton
class StaticFileResolverImpl(json: Json) : StaticFileResolver {
    private val mappings: Map<String, String>

    init {
        val manifest = javaClass.getResource("/css-manifest.json").readText()
        val manifestObject = json.parseToJsonElement(manifest).jsonObject
        val keys = manifestObject.keys
        mappings = keys.map {
            it to "/${manifestObject[it]!!.jsonPrimitive.content}"
        }.toMap()
    }

    override fun resolve(name: String) = mappings[name]
}
