package be.simplenotes.app.api

import be.simplenotes.app.extensions.auto
import be.simplenotes.domain.LoginForm
import be.simplenotes.domain.UserService
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.BAD_REQUEST
import org.http4k.core.Status.Companion.OK
import javax.inject.Singleton

@Singleton
class ApiUserController(
    json: Json,
    private val userService: UserService,
) {
    private val tokenLens = json.auto<Token>().toLens()
    private val loginFormLens = json.auto<LoginForm>().toLens()

    fun login(request: Request) = userService
        .login(loginFormLens(request))
        .fold(
            { Response(BAD_REQUEST) },
            { tokenLens(Token(it), Response(OK)) }
        )
}

@Serializable
data class Token(val token: String)
