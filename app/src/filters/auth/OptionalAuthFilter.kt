package be.simplenotes.app.filters.auth

import be.simplenotes.app.filters.auth.JwtSource.Cookie
import be.simplenotes.domain.security.SimpleJwt
import be.simplenotes.types.LoggedInUser
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.with

class OptionalAuthFilter(
    private val simpleJwt: SimpleJwt<LoggedInUser>,
    private val lens: OptionalAuthLens,
    private val source: JwtSource = Cookie,
) : Filter {
    override fun invoke(next: HttpHandler): HttpHandler = {
        val token = when (source) {
            JwtSource.Header -> it.bearerTokenHeader()
            Cookie -> it.bearerTokenCookie()
        }

        next(it.with(lens of token?.let { simpleJwt.extract(it) }))
    }
}
