package be.simplenotes.app.filters.auth

import be.simplenotes.app.extensions.redirect
import be.simplenotes.domain.security.SimpleJwt
import be.simplenotes.types.LoggedInUser
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Response
import org.http4k.core.Status.Companion.UNAUTHORIZED
import org.http4k.core.with

class RequiredAuthFilter(
    private val simpleJwt: SimpleJwt<LoggedInUser>,
    private val lens: RequiredAuthLens,
    private val source: JwtSource = JwtSource.Cookie,
    private val redirect: Boolean = true,
) : Filter {
    override fun invoke(next: HttpHandler): HttpHandler = {
        val token = when (source) {
            JwtSource.Header -> it.bearerTokenHeader()
            JwtSource.Cookie -> it.bearerTokenCookie()
        }
        val jwtPayload = token?.let { simpleJwt.extract(token) }

        if (jwtPayload != null) next(it.with(lens of jwtPayload))
        else {
            if (redirect) Response.redirect("/login")
            else Response(UNAUTHORIZED)
        }
    }
}
