package be.simplenotes.app.filters.auth

import be.simplenotes.types.LoggedInUser
import org.http4k.core.Request
import org.http4k.core.cookie.cookie
import org.http4k.lens.BiDiLens

typealias OptionalAuthLens = BiDiLens<@JvmSuppressWildcards Request, @JvmSuppressWildcards LoggedInUser?>
typealias RequiredAuthLens = BiDiLens<@JvmSuppressWildcards Request, @JvmSuppressWildcards LoggedInUser>

enum class JwtSource {
    Header, Cookie
}

fun Request.bearerTokenCookie(): String? = cookie("Bearer")
    ?.value
    ?.trim()

fun Request.bearerTokenHeader(): String? =
    header("Authorization")
        ?.trim()
        ?.takeIf { it.startsWith("Bearer") }
        ?.substringAfter("Bearer")
        ?.trim()
