package be.simplenotes.app.filters

import be.simplenotes.app.extensions.html
import be.simplenotes.views.ErrorView
import be.simplenotes.views.ErrorView.Type.*
import org.http4k.core.*
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.NOT_FOUND
import org.http4k.core.Status.Companion.NOT_IMPLEMENTED
import org.http4k.core.Status.Companion.SERVICE_UNAVAILABLE
import org.slf4j.LoggerFactory
import java.sql.SQLTransientException
import javax.inject.Singleton

@Singleton
class ErrorFilter(private val errorView: ErrorView) : Filter {

    private val logger = LoggerFactory.getLogger(javaClass)

    private fun errorResponse(status: Status): Response {
        val type = when (status) {
            SERVICE_UNAVAILABLE -> SqlTransientError
            NOT_FOUND -> NotFound
            NOT_IMPLEMENTED -> Other
            else -> Other
        }

        return Response(status).html(errorView.error(type)).noCache()
    }

    override fun invoke(next: HttpHandler): HttpHandler = { request ->
        try {
            val response = next(request)
            if (response.status == NOT_FOUND) errorResponse(NOT_FOUND)
            else response
        } catch (e: SQLTransientException) {
            logger.error(e.stackTraceToString())
            errorResponse(SERVICE_UNAVAILABLE)
        } catch (e: Exception) {
            logger.error(e.stackTraceToString())
            errorResponse(INTERNAL_SERVER_ERROR)
        } catch (e: NotImplementedError) {
            logger.error(e.stackTraceToString())
            errorResponse(NOT_IMPLEMENTED)
        }
    }
}
