package be.simplenotes.app.filters

import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Status.Companion.OK

object ImmutableFilter : Filter {
    override fun invoke(next: HttpHandler) = { request: Request ->
        val res = next(request)
        if (res.status == OK)
            res.header("Cache-Control", "public, max-age=31536000, immutable")
        else res
    }
}
