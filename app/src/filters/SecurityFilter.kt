package be.simplenotes.app.filters

import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request

object SecurityFilter : Filter {
    override fun invoke(next: HttpHandler): HttpHandler = { request: Request ->
        val response = next(request)
            .header("X-Content-Type-Options", "nosniff")

        if (response.header("Content-Type")?.contains("text/html") == true) {
            response
                .header("Content-Security-Policy", "default-src 'self'")
                .header("Referrer-Policy", "no-referrer")
        } else response
    }
}
