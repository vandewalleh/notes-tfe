package be.simplenotes.app.jetty

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletContextHandler.SESSIONS
import org.eclipse.jetty.servlet.ServletHolder
import org.http4k.core.HttpHandler
import org.http4k.server.Http4kServer
import org.http4k.server.ServerConfig
import org.http4k.servlet.asServlet

class Jetty(private val port: Int, private val server: Server) : ServerConfig {
    constructor(port: Int, vararg inConnectors: ConnectorBuilder) : this(
        port,
        Server().apply {
            inConnectors.forEach { addConnector(it(this)) }
        }
    )

    override fun toServer(http: HttpHandler): Http4kServer {
        server.insertHandler(http.toJettyHandler())

        return object : Http4kServer {
            override fun start(): Http4kServer = apply {
                server.start()
            }

            override fun stop(): Http4kServer = apply { server.stop() }

            override fun port(): Int = if (port > 0) port else server.uri.port
        }
    }
}

fun HttpHandler.toJettyHandler() = ServletContextHandler(SESSIONS).apply {
    addServlet(ServletHolder(this@toJettyHandler.asServlet()), "/*")
}

typealias ConnectorBuilder = (Server) -> ServerConnector
