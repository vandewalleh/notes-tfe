package be.simplenotes.app

import org.http4k.server.Http4kServer
import org.slf4j.LoggerFactory
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import javax.inject.Singleton
import be.simplenotes.config.ServerConfig as SimpleNotesServerConfig

@Singleton
class Server(
    private val config: SimpleNotesServerConfig,
    private val http4kServer: Http4kServer,
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    fun start(): Server {
        http4kServer.start()
        logger.info("Listening on http://${config.host}:${http4kServer.port()}")
        return this
    }

    @PreDestroy
    fun stop() {
        logger.info("Stopping server")
        http4kServer.close()
    }
}
