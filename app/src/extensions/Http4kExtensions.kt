package be.simplenotes.app.extensions

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.http4k.asString
import org.http4k.core.Body
import org.http4k.core.ContentType
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.FOUND
import org.http4k.core.Status.Companion.MOVED_PERMANENTLY
import org.http4k.lens.*

fun Response.html(html: String) = body(html)
    .header("Content-Type", "text/html; charset=utf-8")
    .header("Cache-Control", "no-cache")

fun Response.Companion.redirect(url: String, permanent: Boolean = false) =
    Response(if (permanent) MOVED_PERMANENTLY else FOUND).header("Location", url)

fun Request.isSecure() = header("X-Forwarded-Proto")?.contains("https") ?: false

val bodyLens = httpBodyRoot(
    listOf(Meta(true, "body", ParamMeta.ObjectParam, "body")),
    ContentType.APPLICATION_JSON.withNoDirectives(),
    ContentNegotiation.StrictNoDirective
).map(
    { it.payload.asString() },
    { Body(it) }
)

inline fun <reified T> Json.auto(): BiDiBodyLensSpec<T> = bodyLens.map(
    { decodeFromString(it) },
    { encodeToString(it) }
)
