package be.simplenotes.app

import io.micronaut.context.ApplicationContext
import java.lang.Runtime.getRuntime

fun main() {
    val env = if (System.getenv("ENV") == "dev") "dev" else "prod"
    val ctx = ApplicationContext.builder()
        .deduceEnvironment(false)
        .environments(env)
        .start()
    ctx.createBean(Server::class.java)
    getRuntime().addShutdownHook(Thread { ctx.stop() })
}
