package be.simplenotes.app.modules

import be.simplenotes.app.filters.auth.*
import be.simplenotes.domain.security.SimpleJwt
import be.simplenotes.types.LoggedInUser
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Primary
import org.http4k.core.RequestContexts
import org.http4k.lens.RequestContextKey
import javax.inject.Named
import javax.inject.Singleton

@Factory
class AuthModule {

    @Singleton
    @Named("optional")
    fun optionalAuthLens(ctx: RequestContexts): OptionalAuthLens = RequestContextKey.optional(ctx)

    @Singleton
    @Named("required")
    fun requiredAuthLens(ctx: RequestContexts): RequiredAuthLens = RequestContextKey.required(ctx)

    @Singleton
    fun optionalAuth(simpleJwt: SimpleJwt<LoggedInUser>, @Named("optional") lens: OptionalAuthLens) =
        OptionalAuthFilter(simpleJwt, lens)

    @Primary
    @Singleton
    fun requiredAuth(simpleJwt: SimpleJwt<LoggedInUser>, @Named("required") lens: RequiredAuthLens) =
        RequiredAuthFilter(simpleJwt, lens)

    @Singleton
    @Named("api")
    internal fun apiAuthFilter(
        simpleJwt: SimpleJwt<LoggedInUser>,
        @Named("required") lens: RequiredAuthLens,
    ) = RequiredAuthFilter(
        simpleJwt = simpleJwt,
        lens = lens,
        source = JwtSource.Header,
        redirect = false
    )

    @Singleton
    fun requestContexts() = RequestContexts()
}
