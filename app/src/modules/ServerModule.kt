package be.simplenotes.app.modules

import be.simplenotes.app.jetty.ConnectorBuilder
import be.simplenotes.app.jetty.Jetty
import be.simplenotes.app.routes.Router
import be.simplenotes.app.utils.StaticFileResolver
import be.simplenotes.config.ServerConfig
import io.micronaut.context.annotation.Factory
import org.eclipse.jetty.server.ServerConnector
import org.http4k.server.Http4kServer
import org.http4k.server.asServer
import javax.inject.Named
import javax.inject.Singleton
import org.eclipse.jetty.server.Server as JettyServer
import org.http4k.server.ServerConfig as Http4kServerConfig

@Factory
class ServerModule {

    @Singleton
    @Named("styles")
    fun styles(resolver: StaticFileResolver) = resolver.resolve("styles.css")!!

    @Singleton
    fun http4kServer(router: Router, serverConfig: Http4kServerConfig): Http4kServer =
        router().asServer(serverConfig)

    @Singleton
    fun http4kServerConfig(config: ServerConfig): Http4kServerConfig {
        val builder: ConnectorBuilder = { server: JettyServer ->
            ServerConnector(server).apply {
                port = config.port
                host = config.host
            }
        }
        return Jetty(config.port, builder)
    }
}
