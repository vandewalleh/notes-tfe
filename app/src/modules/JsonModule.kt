package be.simplenotes.app.modules

import be.simplenotes.app.serialization.LocalDateTimeSerializer
import be.simplenotes.app.serialization.UuidSerializer
import io.micronaut.context.annotation.Factory
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import java.time.LocalDateTime
import java.util.*
import javax.inject.Singleton

@Factory
class JsonModule {

    @Singleton
    fun json() = Json {
        prettyPrint = true
        serializersModule = SerializersModule {
            contextual(LocalDateTime::class, LocalDateTimeSerializer())
            contextual(UUID::class, UuidSerializer())
        }
    }
}
