package be.simplenotes.app.routes

import be.simplenotes.app.controllers.NoteController
import be.simplenotes.app.filters.auth.RequiredAuthFilter
import be.simplenotes.app.filters.auth.RequiredAuthLens
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.then
import org.http4k.routing.PathMethod
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes
import java.util.function.Supplier
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class NoteRoutes(
    private val noteCtrl: NoteController,
    private val auth: RequiredAuthFilter,
    @Named("required") private val authLens: RequiredAuthLens,
) : Supplier<RoutingHttpHandler> {
    override fun get(): RoutingHttpHandler {

        infix fun PathMethod.to(action: ProtectedHandler) =
            this to { req: Request -> action(req, authLens(req)) }

        return auth.then(
            with(noteCtrl) {
                routes(
                    "/" bind GET to ::list,
                    "/" bind POST to ::search,
                    "/new" bind GET to ::new,
                    "/new" bind POST to ::new,
                    "/trash" bind GET to ::trash,
                    "/{uuid}" bind GET to ::note,
                    "/{uuid}" bind POST to ::note,
                    "/{uuid}/edit" bind GET to ::edit,
                    "/{uuid}/edit" bind POST to ::edit,
                    "/deleted/{uuid}" bind POST to ::deleted,
                ).withBasePath("/notes")
            }
        )
    }
}
