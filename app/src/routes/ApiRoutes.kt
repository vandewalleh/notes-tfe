package be.simplenotes.app.routes

import be.simplenotes.app.api.ApiNoteController
import be.simplenotes.app.api.ApiUserController
import be.simplenotes.app.filters.auth.RequiredAuthFilter
import be.simplenotes.app.filters.auth.RequiredAuthLens
import org.http4k.core.Method.*
import org.http4k.core.Request
import org.http4k.core.then
import org.http4k.routing.PathMethod
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes
import java.util.function.Supplier
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class ApiRoutes(
    private val apiUserController: ApiUserController,
    private val apiNoteController: ApiNoteController,
    @Named("api") private val auth: RequiredAuthFilter,
    @Named("required") private val authLens: RequiredAuthLens,
) : Supplier<RoutingHttpHandler> {
    override fun get(): RoutingHttpHandler {

        infix fun PathMethod.to(action: ProtectedHandler) =
            this to { req: Request -> action(req, authLens(req)) }

        return routes(
            "/login" bind POST to apiUserController::login,

            with(apiNoteController) {
                auth.then(
                    routes(
                        "/" bind GET to ::notes,
                        "/" bind POST to ::createNote,
                        "/search" bind POST to ::search,
                        "/{uuid}" bind GET to ::note,
                        "/{uuid}" bind PUT to ::update,
                    )
                ).withBasePath("/notes")
            }

        ).withBasePath("/api")
    }
}
