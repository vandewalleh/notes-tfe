package be.simplenotes.app.routes

import be.simplenotes.app.filters.ErrorFilter
import be.simplenotes.app.filters.SecurityFilter
import org.http4k.core.RequestContexts
import org.http4k.core.then
import org.http4k.filter.ResponseFilters.GZip
import org.http4k.filter.ServerFilters.InitialiseRequestContext
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.routes
import java.util.function.Supplier
import javax.inject.Singleton

@Singleton
class Router(
    private val errorFilter: ErrorFilter,
    private val contexts: RequestContexts,
    private val subRouters: List<Supplier<RoutingHttpHandler>>,
) {
    operator fun invoke(): RoutingHttpHandler {

        val routes = routes(
            *subRouters.map { it.get() }.toTypedArray()
        )

        return errorFilter
            .then(InitialiseRequestContext(contexts))
            .then(SecurityFilter)
            .then(GZip())
            .then(routes)
    }
}
