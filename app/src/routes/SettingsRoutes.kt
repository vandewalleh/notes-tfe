package be.simplenotes.app.routes

import be.simplenotes.app.controllers.SettingsController
import be.simplenotes.app.filters.auth.RequiredAuthFilter
import be.simplenotes.app.filters.auth.RequiredAuthLens
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.then
import org.http4k.routing.PathMethod
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes
import java.util.function.Supplier
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class SettingsRoutes(
    private val settingsController: SettingsController,
    private val auth: RequiredAuthFilter,
    @Named("required") private val authLens: RequiredAuthLens,
) : Supplier<RoutingHttpHandler> {
    override fun get(): RoutingHttpHandler {

        infix fun PathMethod.to(action: ProtectedHandler) =
            this to { req: Request -> action(req, authLens(req)) }

        return auth.then(
            routes(
                "/settings" bind GET to settingsController::settings,
                "/settings" bind POST to settingsController::settings,
                "/export" bind POST to settingsController::export,
            )
        )
    }
}
