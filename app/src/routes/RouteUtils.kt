package be.simplenotes.app.routes

import be.simplenotes.types.LoggedInUser
import org.http4k.core.Request
import org.http4k.core.Response

internal typealias PublicHandler = (Request, LoggedInUser?) -> Response
internal typealias ProtectedHandler = (Request, LoggedInUser) -> Response
