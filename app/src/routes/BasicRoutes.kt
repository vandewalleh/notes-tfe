package be.simplenotes.app.routes

import be.simplenotes.app.controllers.BaseController
import be.simplenotes.app.controllers.NoteController
import be.simplenotes.app.controllers.UserController
import be.simplenotes.app.filters.ImmutableFilter
import be.simplenotes.app.filters.auth.OptionalAuthFilter
import be.simplenotes.app.filters.auth.OptionalAuthLens
import org.http4k.core.ContentType
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.then
import org.http4k.routing.*
import java.util.function.Supplier
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class BasicRoutes(
    private val baseCtrl: BaseController,
    private val userCtrl: UserController,
    private val noteCtrl: NoteController,
    @Named("optional") private val authLens: OptionalAuthLens,
    private val auth: OptionalAuthFilter,
) : Supplier<RoutingHttpHandler> {

    override fun get(): RoutingHttpHandler {

        infix fun PathMethod.to(action: PublicHandler) =
            this to { req: Request -> action(req, authLens(req)) }

        val staticHandler = ImmutableFilter.then(
            static(
                ResourceLoader.Classpath("/static"),
                "woff2" to ContentType("font/woff2"),
                "webmanifest" to ContentType("application/manifest+json")
            )
        )

        return routes(
            auth.then(
                routes(
                    "/" bind GET to baseCtrl::index,
                    "/register" bind GET to userCtrl::register,
                    "/register" bind POST to userCtrl::register,
                    "/login" bind GET to userCtrl::login,
                    "/login" bind POST to userCtrl::login,
                    "/logout" bind POST to userCtrl::logout,
                    "/notes/public/{uuid}" bind GET to noteCtrl::public,
                )
            ),
            staticHandler
        )
    }
}
