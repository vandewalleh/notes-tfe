document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('#note pre code').forEach((b) => {
        hljs.highlightBlock(b);
    });
});
