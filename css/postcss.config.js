module.exports = {
    plugins: [
        require('postcss-import'),
        require('postcss-nested'),
        require('tailwindcss'),
        require('autoprefixer'),
        require('cssnano')({
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }]
        }),
        require('postcss-hash')({
            algorithm: 'sha256',
            trim: 20,
            manifest: process.env.MANIFEST
        }),
    ]
}
