module.exports = {
    purge: {
        content: [
            process.env.PURGE
        ]
    },
    theme: {
        fontFamily: {
            'sans': [
                'Recursive',
                'sans-serif',
            ],
            'mono': [
                'Recursive',
                'monospace'
            ],
        }
    },
    variants: {},
    plugins: [],
}
