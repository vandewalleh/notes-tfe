from typing import List


class NoteMetadata:
    def __init__(self, uuid: str, title: str, tags: List[str], updatedAt: str):
        self.uuid = uuid
        self.title = title
        self.tags = tags
        self.updated_at = updatedAt

    def __str__(self) -> str:
        return f"NoteMetadata(uuid={self.uuid}, title={self.title}, tags={self.tags})"
