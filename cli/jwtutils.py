from datetime import datetime

import jwt


def is_expired(token: str) -> bool:
    exp = jwt.decode(token, verify=False)["exp"]
    dt = datetime.utcfromtimestamp(exp)
    now = datetime.utcnow()
    return dt < now
