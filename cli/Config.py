import configparser
import os
from pathlib import Path

import appdirs
import click


class Config:
    def __init__(self):
        config_dir: str = appdirs.user_config_dir("SimpleNotesCli")
        Path(config_dir).mkdir(exist_ok=True)
        self.file = os.path.join(config_dir, "config.ini")
        self.configparser = configparser.ConfigParser()

        self.token = None
        self.base_url = None

    def load(self):
        self.configparser.read(self.file)
        self.token = self.configparser.get("DEFAULT", "token", fallback=None)
        self.base_url = self.configparser.get(
            "DEFAULT", "base_url", fallback="https://simplenotes.be"
        )

    def save(self):
        if self.token:
            self.configparser.set("DEFAULT", "token", self.token)

        if self.base_url:
            self.configparser.set("DEFAULT", "base_url", self.base_url)

        try:
            with open(self.file, "w") as f:
                self.configparser.write(f)
        except IOError:
            click.secho("An error occurred while saving config", fg="red", err=True)
            exit(1)
