from typing import List, Optional

import click
import requests
from requests.models import Response

from domain import NoteMetadata


class SimplenotesApi:
    def __init__(self, base_url: str):
        self.base_url = base_url
        self.s = requests.Session()
        self.s.hooks["response"] = [self.__exit_unauthorized]

    def __url(self, path: str) -> str:
        return f"{self.base_url}{path}"

    def __exit_unauthorized(self, response: Response, *args, **kwargs):
        if response.status_code == 401:
            click.secho("Unauthorized, please login again", fg="red", err=True)
            exit(1)

    def login(self, username: str, password: str) -> Optional[str]:
        url = self.__url("/api/login")
        r = self.s.post(
            url,
            json={"username": username, "password": password},
        )
        if r.status_code == 200:
            return r.json()["token"]

    def set_token(self, token: str):
        self.s.headers.update({"Authorization": f"Bearer {token}"})

    def find_note(self, uuid: str) -> Optional[str]:
        url = self.__url(f"/api/notes/{uuid}")
        r = self.s.get(url)
        if r.status_code == 200:
            return r.json()["markdown"]

    def list_notes(self) -> List[NoteMetadata]:
        url = self.__url("/api/notes")
        r = self.s.get(url)
        return list(map(lambda x: NoteMetadata(**x), r.json()))

    def search_notes(self, query: str) -> List[NoteMetadata]:
        url = self.__url("/api/notes/search")
        r = self.s.post(url, json={"query": query})

        if r.status_code == 200:
            j = r.json()
            return list(map(lambda x: NoteMetadata(**x), j))
        else:
            return []

    def create_note(self, content: str) -> Optional[str]:
        url = self.__url("/api/notes/")
        r = self.s.post(url, json={"content": content})
        if r.status_code == 200:
            return r.json()["uuid"]

    def update_note(self, uuid: str, content: str) -> bool:
        url = self.__url(f"/api/notes/{uuid}")
        r = self.s.put(url, json={"content": content})
        return r.status_code == 200
