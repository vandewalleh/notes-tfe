from typing import List

import editor
import click
from tabulate import tabulate

from domain import NoteMetadata


def edit_md(content: str = "") -> str:
    b = bytes(content, "utf-8")
    result = editor.edit(contents=b, suffix=".md")
    return result.decode("utf-8")


def print_notes(notes: List[NoteMetadata]):
    data = []
    for n in notes:
        uuid = click.style(n.uuid, fg="blue")
        title = click.style(n.title, fg="green", bold=True)
        tags = ["#" + e for e in n.tags]
        tags = " ".join(tags)
        data.append([uuid, title, tags])

    headers = ["UUID", "title", "tags"]
    click.echo(tabulate(data, headers=headers))


def print_note_url(uuid: str, action: str, base_url: str):
    url = f"{base_url}/notes/{uuid}"
    s = click.style(url, fg="green", underline=True)
    click.echo(f"Note {action}: {s}")
