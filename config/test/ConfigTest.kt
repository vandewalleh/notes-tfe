package be.simplenotes.config

import io.micronaut.context.ApplicationContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ConfigTest {
    private val ctx = ApplicationContext.run()

    @Test
    fun `check application yaml is on the classpath`() {
        val yaml = javaClass.getResource("/application.yaml")
        assertThat(yaml).`as`("The application.yaml resource").isNotNull
        assertThat(yaml.readText()).`as`("The config content").isNotBlank
    }

    @Test
    fun `check config properties`() {
        assertThat(ctx.getProperty("jwt.validity", Int::class.java))
            .`as`("Jwt.validity")
            .isPresent
    }

    @Test
    fun `load jwt config`() {
        val jwtConfig = ctx.getBean(JwtConfig::class.java)
        assertThat(jwtConfig).isNotNull
    }
}
