package be.simplenotes.config

import io.micronaut.context.annotation.*
import java.nio.file.Path
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

data class DataConfig(val dataDir: String)

data class DataSourceConfig(
    val jdbcUrl: String,
    val maximumPoolSize: Int,
    val connectionTimeout: Long,
)

@ConfigurationProperties("jwt")
data class JwtConfig @ConfigurationInject constructor(
    val secret: String,
    val validity: Long,
    val timeUnit: TimeUnit,
) {
    override fun toString() = "JwtConfig(secret='***', validity=$validity, timeUnit=$timeUnit)"
}

@ConfigurationProperties("server")
data class ServerConfig @ConfigurationInject constructor(
    val host: String,
    val port: Int,
)

@Factory
class ConfigFactory {

    @Singleton
    @Requires(notEnv = ["test"])
    fun datasourceConfig(dataConfig: DataConfig) = DataSourceConfig(
        jdbcUrl = "jdbc:h2:" + Path.of(dataConfig.dataDir, "simplenotes").normalize().toAbsolutePath(),
        maximumPoolSize = 10,
        connectionTimeout = 1000
    )

    @Singleton
    @Requires(env = ["test"])
    fun testDatasourceConfig() = DataSourceConfig(
        jdbcUrl = "jdbc:h2:mem:regular;DB_CLOSE_DELAY=-1",
        maximumPoolSize = 2,
        connectionTimeout = 1000
    )

    @Singleton
    @Requires(notEnv = ["test"])
    fun dataConfig(@Property(name = "data-dir") dataDir: String) = DataConfig(dataDir)

    @Singleton
    @Requires(env = ["test"])
    fun testDataConfig() = DataConfig("/tmp")

}
