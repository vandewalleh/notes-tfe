import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.micronaut")
}

dependencies {
    micronaut()

    testImplementation(libs.bundles.test)
    testRuntimeOnly(libs.slf4j.logback)
}
