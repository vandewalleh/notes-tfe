package be.simplenotes.domain.security

import org.mindrot.jbcrypt.BCrypt
import javax.inject.Inject
import javax.inject.Singleton

internal interface PasswordHash {
    fun crypt(password: String): String
    fun verify(password: String, hashedPassword: String): Boolean
}

@Singleton
internal class BcryptPasswordHash constructor(test: Boolean) : PasswordHash {
    @Inject
    constructor() : this(false)

    private val rounds = if (test) 4 else 10
    override fun crypt(password: String) = BCrypt.hashpw(password, BCrypt.gensalt(rounds))!!
    override fun verify(password: String, hashedPassword: String) = BCrypt.checkpw(password, hashedPassword)
}
