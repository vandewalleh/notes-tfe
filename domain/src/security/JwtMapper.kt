package be.simplenotes.domain.security

import be.simplenotes.types.LoggedInUser
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.interfaces.DecodedJWT
import javax.inject.Singleton

interface JwtMapper<T> {
    fun extract(decodedJWT: DecodedJWT): T?
    fun build(builder: JWTCreator.Builder, value: T)
}

@Singleton
class UserJwtMapper : JwtMapper<LoggedInUser> {
    private val userIdField = "i"
    private val usernameField = "u"

    override fun extract(decodedJWT: DecodedJWT): LoggedInUser? {
        val id = decodedJWT.getClaim(userIdField).asInt() ?: null
        val username = decodedJWT.getClaim(usernameField).asString() ?: null
        return if (id != null && username != null)
            LoggedInUser(id, username)
        else null
    }

    override fun build(builder: JWTCreator.Builder, value: LoggedInUser) {
        builder.withClaim(userIdField, value.userId)
            .withClaim(usernameField, value.username)
    }
}
