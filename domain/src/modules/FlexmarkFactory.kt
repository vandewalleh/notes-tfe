package be.simplenotes.domain.modules

import com.vladsch.flexmark.ext.gfm.tasklist.TaskListExtension
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.data.MutableDataSet
import io.micronaut.context.annotation.Factory
import javax.inject.Singleton

@Factory
class FlexmarkFactory {

    @Singleton
    fun parser(options: MutableDataSet) = Parser.builder(options).build()

    @Singleton
    fun htmlRenderer(options: MutableDataSet) = HtmlRenderer.builder(options).build()

    @Singleton
    fun options() = MutableDataSet().apply {
        set(Parser.EXTENSIONS, listOf(TaskListExtension.create()))
        set(TaskListExtension.TIGHT_ITEM_CLASS, "")
        set(TaskListExtension.LOOSE_ITEM_CLASS, "")
        set(
            TaskListExtension.ITEM_DONE_MARKER,
            """<input type="checkbox" checked="checked" disabled="disabled" readonly="readonly" />&nbsp;"""
        )
        set(
            TaskListExtension.ITEM_NOT_DONE_MARKER,
            """<input type="checkbox" disabled="disabled" readonly="readonly" />&nbsp;"""
        )
        set(HtmlRenderer.SOFT_BREAK, "<br>")
    }
}
