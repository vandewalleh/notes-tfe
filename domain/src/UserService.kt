package be.simplenotes.domain

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.filterOrElse
import arrow.core.leftIfNull
import arrow.core.rightIfNotNull
import be.simplenotes.domain.security.PasswordHash
import be.simplenotes.domain.security.SimpleJwt
import be.simplenotes.domain.validation.UserValidations
import be.simplenotes.persistence.repositories.UserRepository
import be.simplenotes.search.NoteSearcher
import be.simplenotes.types.LoggedInUser
import be.simplenotes.types.PersistedUser
import io.konform.validation.ValidationErrors
import kotlinx.serialization.Serializable
import javax.inject.Singleton

interface UserService {
    fun register(form: RegisterForm): Either<RegisterError, PersistedUser>
    fun login(form: LoginForm): Either<LoginError, Token>
    fun delete(form: DeleteForm): Either<DeleteError, Unit>
}

@Singleton
internal class UserServiceImpl(
    private val userRepository: UserRepository,
    private val passwordHash: PasswordHash,
    private val jwt: SimpleJwt<LoggedInUser>,
    private val searcher: NoteSearcher,
) : UserService {

    override fun register(form: RegisterForm) = UserValidations.validateRegister(form)
        .filterOrElse({ !userRepository.exists(it.username) }, { RegisterError.UserExists })
        .map { it.copy(password = passwordHash.crypt(it.password)) }
        .map { userRepository.create(it) }
        .leftIfNull { RegisterError.UserExists }

    override fun login(form: LoginForm) = either.eager<LoginError, Token> {
        UserValidations.validateLogin(form)
            .bind()
            .let { userRepository.find(it.username) }
            .rightIfNotNull { LoginError.Unregistered }
            .filterOrElse({ passwordHash.verify(form.password!!, it.password) }, { LoginError.WrongPassword })
            .map { jwt.sign(LoggedInUser(it)) }
            .bind()
    }

    override fun delete(form: DeleteForm) = either.eager<DeleteError, Unit> {
        val user = !UserValidations.validateDelete(form)
        val persistedUser = !userRepository.find(user.username).rightIfNotNull { DeleteError.Unregistered }
        !Either.conditionally(
            passwordHash.verify(user.password, persistedUser.password),
            { DeleteError.WrongPassword },
            { }
        )
        !Either.conditionally(userRepository.delete(persistedUser.id), { DeleteError.Unregistered }, { })
        searcher.dropIndex(persistedUser.id)
    }
}

sealed class DeleteError {
    object Unregistered : DeleteError()
    object WrongPassword : DeleteError()
    class InvalidForm(val validationErrors: ValidationErrors) : DeleteError()
}

data class DeleteForm(val username: String?, val password: String?, val checked: Boolean)

sealed class LoginError {
    object Unregistered : LoginError()
    object WrongPassword : LoginError()
    class InvalidLoginForm(val validationErrors: ValidationErrors) : LoginError()
}

typealias Token = String

sealed class RegisterError {
    object UserExists : RegisterError()
    class InvalidRegisterForm(val validationErrors: ValidationErrors) : RegisterError()
}

typealias RegisterForm = LoginForm

@Serializable
data class LoginForm(val username: String?, val password: String?)
