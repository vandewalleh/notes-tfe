package be.simplenotes.domain.validation

import be.simplenotes.domain.MarkdownParsingError
import be.simplenotes.types.NoteMetadata
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxItems
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.uniqueItems

internal object NoteValidations {
    private val metaValidator = Validation<NoteMetadata> {
        NoteMetadata::title required {
            addConstraint("must not be blank") { it.isNotBlank() }
            maxLength(50)
        }
        NoteMetadata::tags required {
            maxItems(5)
            uniqueItems(true)
        }

        NoteMetadata::tags onEach {
            maxLength(15)
            addConstraint("must not be blank") { it.isNotBlank() }
            addConstraint("must only contain alphanumeric characters, `-` and `_`") {
                it.matches("^[a-zA-Z0-9-_]+\$".toRegex())
            }
        }
    }

    fun validateMetadata(meta: NoteMetadata): MarkdownParsingError.ValidationError? {
        val errors = metaValidator.validate(meta).errors
        return if (errors.isEmpty()) null
        else return MarkdownParsingError.ValidationError(errors)
    }
}
