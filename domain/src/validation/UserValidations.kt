package be.simplenotes.domain.validation

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import be.simplenotes.domain.*
import be.simplenotes.types.User
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength

internal object UserValidations {
    private val loginValidator = Validation<LoginForm> {
        LoginForm::username required {
            minLength(3)
            maxLength(50)
        }
        LoginForm::password required {
            minLength(8)
            maxLength(72) // jbcrypt limit, see https://security.stackexchange.com/a/39851
        }
    }

    fun validateLogin(form: LoginForm): Either<LoginError.InvalidLoginForm, User> {
        val errors = loginValidator.validate(form).errors
        return if (errors.isEmpty()) User(form.username!!, form.password!!).right()
        else return LoginError.InvalidLoginForm(errors).left()
    }

    fun validateRegister(form: RegisterForm): Either<RegisterError.InvalidRegisterForm, User> {
        val errors = loginValidator.validate(form).errors
        return if (errors.isEmpty()) User(form.username!!, form.password!!).right()
        else return RegisterError.InvalidRegisterForm(errors).left()
    }

    private val deleteValidator = Validation<DeleteForm> {
        DeleteForm::username required {
            minLength(3)
            maxLength(50)
        }
        DeleteForm::password required {
            minLength(8)
            maxLength(72)
        }
        DeleteForm::checked required {
            addConstraint("Should be checked") { it }
        }
    }

    fun validateDelete(form: DeleteForm): Either<DeleteError.InvalidForm, User> {
        val errors = deleteValidator.validate(form).errors
        return if (errors.isEmpty()) User(form.username!!, form.password!!).right()
        else return DeleteError.InvalidForm(errors).left()
    }
}
