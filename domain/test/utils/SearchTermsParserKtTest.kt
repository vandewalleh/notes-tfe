package be.simplenotes.domain.utils

import be.simplenotes.search.SearchTerms
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class SearchTermsParserKtTest {

    private fun createResult(
        input: String,
        title: String? = null,
        tag: String? = null,
        content: String? = null,
        all: String? = null,
    ): Pair<String, SearchTerms> = input to SearchTerms(title, tag, content, all)

    @Suppress("Unused")
    private fun results() = Stream.of(
        createResult("title:'example'", title = "example"),
        createResult("title:'example with words'", title = "example with words"),
        createResult("title:'example with words'", title = "example with words"),
        createResult("""title:"double quotes"""", title = "double quotes"),
        createResult("title:'example' something else", title = "example", all = "something else"),
        createResult("tag:'example'", tag = "example"),
        createResult("tag:'example' title:'other'", title = "other", tag = "example"),
        createResult("blah blah tag:'example' title:'other'", title = "other", tag = "example", all = "blah blah"),
        createResult("tag:'example' middle title:'other'", title = "other", tag = "example", all = "middle"),
        createResult("tag:'example' title:'other' end", title = "other", tag = "example", all = "end"),
        createResult(
            "tag:'example abc' title:'other with words' this is the end  ",
            title = "other with words",
            tag = "example abc",
            all = "this is the end"
        ),
        createResult("tag:blah", tag = "blah"),
        createResult("tag:'some words'", tag = "some words"),
        createResult("tag:'some words ' global", tag = "some words ", all = "global"),
        createResult(
            "tag:'double quote inside single \" ' global",
            tag = "double quote inside single \" ",
            all = "global"
        ),
    )

    @ParameterizedTest
    @MethodSource("results")
    fun `valid search parser`(case: Pair<String, SearchTerms>) {
        assertThat(parseSearchTerms(case.first), equalTo(case.second))
    }
}
