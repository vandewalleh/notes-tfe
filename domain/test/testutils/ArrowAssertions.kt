package be.simplenotes.domain.testutils

import arrow.core.Either
import com.natpryce.hamkrest.MatchResult
import com.natpryce.hamkrest.Matcher

fun isLeft() = object : Matcher<Either<*, *>> {
    override val description: String
        get() = "is Either.Left<>"

    override fun invoke(actual: Either<*, *>) = when {
        actual.isLeft() -> MatchResult.Match
        else -> MatchResult.Mismatch("is Either.Right<>")
    }
}

fun isRight() = object : Matcher<Either<*, *>> {
    override val description: String
        get() = "is Either.Right<>"

    override fun invoke(actual: Either<*, *>) = when (actual) {
        is Either.Right -> MatchResult.Match
        is Either.Left -> {
            val valueA = actual.a
            MatchResult.Mismatch("is Either.Left<${if (valueA == null) "Null" else valueA::class.simpleName}>")
        }
    }
}

inline fun <reified A> isLeftOfType() = object : Matcher<Either<*, *>> {
    override val description: String
        get() = "is Either.Left<${A::class.qualifiedName}>"

    override fun invoke(actual: Either<*, *>) = when (actual) {
        is Either.Right -> MatchResult.Mismatch("was Either.Right<>")
        is Either.Left -> {
            val valueA = actual.a
            if (valueA is A) MatchResult.Match
            else MatchResult.Mismatch("was Left<${if (valueA == null) "Null" else valueA::class.simpleName}>")
        }
    }
}
