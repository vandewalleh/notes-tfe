package be.simplenotes.domain.security

import be.simplenotes.config.JwtConfig
import be.simplenotes.domain.Token
import be.simplenotes.types.LoggedInUser
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.natpryce.hamkrest.absent
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

internal class LoggedInUserExtractorTest {
    private val jwtConfig = JwtConfig("a secret", 1, TimeUnit.HOURS)
    private val mapper = UserJwtMapper()
    private val simpleJwt = SimpleJwt(jwtConfig, mapper)

    private fun createToken(username: String? = null, id: Int? = null, secret: String = jwtConfig.secret): Token {
        val algo = Algorithm.HMAC256(secret)
        return JWT.create().apply {
            if (username != null && id != null) mapper.build(this, LoggedInUser(id, username))
        }.sign(algo)
    }

    @Suppress("Unused")
    private fun invalidTokens() = Stream.of(
        createToken(id = 1),
        createToken(username = "user"),
        createToken(),
        createToken(username = "user", id = 1, secret = "not the correct secret"),
        createToken(username = "user", id = 1) + "\"efesfsef",
        "something that is not even a token"
    )

    @ParameterizedTest(name = "[{index}] token `{0}` should be invalid")
    @MethodSource("invalidTokens")
    fun `parse invalid tokens`(token: String) {
        assertThat(simpleJwt.extract(token), absent())
    }

    @Test
    fun `parse valid token`() {
        val token = createToken(username = "someone", id = 1)
        assertThat(simpleJwt.extract(token), equalTo(LoggedInUser(1, "someone")))
    }
}
