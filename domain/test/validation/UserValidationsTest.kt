package be.simplenotes.domain.validation

import be.simplenotes.domain.LoginError
import be.simplenotes.domain.LoginForm
import be.simplenotes.domain.RegisterForm
import be.simplenotes.domain.testutils.isLeftOfType
import be.simplenotes.domain.testutils.isRight
import com.natpryce.hamkrest.assertion.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class UserValidationsTest {

    @Nested
    inner class Login {

        @Suppress("Unused")
        fun invalidLoginForms(): Stream<LoginForm> = Stream.of(
            LoginForm(username = null, password = null),
            LoginForm(username = "", password = ""),
            LoginForm(username = "a", password = "aaaa"),
            LoginForm(username = "a".repeat(51), password = "a".repeat(8)),
            LoginForm(username = "a".repeat(10), password = "a".repeat(7))
        )

        @ParameterizedTest
        @MethodSource("invalidLoginForms")
        fun `validate invalid logins`(form: LoginForm) {
            assertThat(UserValidations.validateLogin(form), isLeftOfType<LoginError.InvalidLoginForm>())
        }

        @Suppress("Unused")
        fun validLoginForms(): Stream<LoginForm> = Stream.of(
            LoginForm(username = "a".repeat(50), password = "a".repeat(72)),
            LoginForm(username = "a".repeat(3), password = "a".repeat(8))
        )

        @ParameterizedTest
        @MethodSource("validLoginForms")
        fun `validate valid logins`(form: LoginForm) {
            assertThat(UserValidations.validateLogin(form), isRight())
        }
    }

    @Nested
    inner class Register {

        @Suppress("Unused")
        fun invalidRegisterForms(): Stream<RegisterForm> = Stream.of(
            RegisterForm(username = null, password = null),
            RegisterForm(username = "", password = ""),
            RegisterForm(username = "a", password = "aaaa"),
            RegisterForm(username = "a".repeat(51), password = "a".repeat(8)),
            RegisterForm(username = "a".repeat(10), password = "a".repeat(7))
        )

        @ParameterizedTest
        @MethodSource("invalidRegisterForms")
        fun `validate invalid register`(form: LoginForm) {
            assertThat(UserValidations.validateLogin(form), isLeftOfType<LoginError.InvalidLoginForm>())
        }

        @Suppress("Unused")
        fun validRegisterForms(): Stream<RegisterForm> = Stream.of(
            RegisterForm(username = "a".repeat(50), password = "a".repeat(72)),
            RegisterForm(username = "a".repeat(3), password = "a".repeat(8))
        )

        @ParameterizedTest
        @MethodSource("validRegisterForms")
        fun `validate valid register`(form: LoginForm) {
            assertThat(UserValidations.validateLogin(form), isRight())
        }
    }
}
