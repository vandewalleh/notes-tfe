import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.kotlinx-serialization")
    id("be.simplenotes.micronaut")
}

dependencies {
    api(project(":config"))
    api(project(":types"))
    implementation(project(":persistence"))
    implementation(project(":search"))

    api(libs.arrow.core.data)
    api(libs.konform)

    micronaut()

    implementation(libs.kotlinx.serialization.json)
    implementation(libs.bcrypt)
    implementation(libs.jwt)
    implementation(libs.bundles.flexmark)
    implementation(libs.yaml)
    implementation(libs.owasp.html.sanitizer)
    implementation(libs.commonsCompress)

    testImplementation(libs.bundles.test)
}
