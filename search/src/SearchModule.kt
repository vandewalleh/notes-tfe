package be.simplenotes.search

import be.simplenotes.config.DataConfig
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Prototype
import java.nio.file.Path
import javax.inject.Named

@Factory
class SearchModule {

    @Named("search-index")
    @Prototype
    internal fun luceneIndex(dataConfig: DataConfig) = Path.of(dataConfig.dataDir).resolve("index")
}
