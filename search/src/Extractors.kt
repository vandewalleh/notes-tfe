package be.simplenotes.search

import be.simplenotes.types.PersistedNote
import be.simplenotes.types.PersistedNoteMetadata
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.document.StringField
import org.apache.lucene.document.TextField

internal fun PersistedNote.toDocument() = Document().apply {
    // non searchable fields
    add(StringField(uuidField, UuidFieldConverter.toDoc(uuid), Field.Store.YES))
    add(StringField(updatedAtField, LocalDateTimeFieldConverter.toDoc(updatedAt), Field.Store.YES))

    // searchable fields
    add(TextField(titleField, title, Field.Store.YES))
    add(TextField(tagsField, TagsFieldConverter.toDoc(tags), Field.Store.YES))
    add(TextField(contentField, markdown, Field.Store.YES))
}

internal fun Document.toNoteMeta() = PersistedNoteMetadata(
    title = get(titleField),
    uuid = UuidFieldConverter.fromDoc(get(uuidField)),
    updatedAt = LocalDateTimeFieldConverter.fromDoc(get(updatedAtField)),
    tags = TagsFieldConverter.fromDoc(get(tagsField))
)
