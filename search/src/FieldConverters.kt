package be.simplenotes.search

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

internal interface FieldConverter<T> {
    fun toDoc(value: T): String
    fun fromDoc(value: String): T
}

internal object LocalDateTimeFieldConverter : FieldConverter<LocalDateTime> {
    private val formatter = DateTimeFormatter.ISO_DATE_TIME
    override fun toDoc(value: LocalDateTime): String = formatter.format(value)
    override fun fromDoc(value: String): LocalDateTime = LocalDateTime.parse(value, formatter)
}

internal object UuidFieldConverter : FieldConverter<UUID> {
    override fun toDoc(value: UUID): String = value.toString()
    override fun fromDoc(value: String): UUID = UUID.fromString(value)
}

internal object TagsFieldConverter : FieldConverter<List<String>> {
    override fun toDoc(value: List<String>): String = value.joinToString(" ")
    override fun fromDoc(value: String): List<String> = value.split(" ").filter(String::isNotEmpty)
}
