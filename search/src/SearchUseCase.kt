package be.simplenotes.search

import be.simplenotes.types.PersistedNote
import be.simplenotes.types.PersistedNoteMetadata
import java.util.*

data class SearchTerms(val title: String?, val tag: String?, val content: String?, val all: String?)

interface NoteSearcher {
    fun indexNote(userId: Int, note: PersistedNote)
    fun indexNotes(userId: Int, notes: List<PersistedNote>)
    fun deleteIndex(userId: Int, uuid: UUID)
    fun updateIndex(userId: Int, note: PersistedNote)
    fun search(userId: Int, terms: SearchTerms): List<PersistedNoteMetadata>
    fun dropIndex(userId: Int)
    fun dropAll()
}
