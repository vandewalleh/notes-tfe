import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.micronaut")
}

dependencies {
    implementation(project(":types"))
    implementation(project(":config"))

    implementation(libs.bundles.lucene)
    implementation(libs.slf4j.api)

    micronaut()

    testImplementation(libs.bundles.test)
}
