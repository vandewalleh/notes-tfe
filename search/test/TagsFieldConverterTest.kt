package be.simplenotes.search

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class TagsFieldConverterTest {

    @Suppress("unused")
    fun tags(): Stream<List<String>> = Stream.of(
        listOf("example"),
        listOf("example", "second"),
        listOf(),
    )

    @ParameterizedTest
    @MethodSource("tags")
    fun `tags should stay the same`(input: List<String>) {
        val doc = TagsFieldConverter.toDoc(input)
        val out = TagsFieldConverter.fromDoc(doc)
        assertThat(out)
            .hasSameSizeAs(input)
            .containsExactlyInAnyOrderElementsOf(input)
    }
}
