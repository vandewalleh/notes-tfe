# SimpleNotes, a simple markdown note taking website

## Requirements

- Docker
- docker-compose

## How to run

- Copy the docker-compose.yml somewhere
- In the same directory, copy the *.env.dist* file and rename it to *.env*
- Edit the variables inside *.env* (see below)
- Run it with `docker-compose up -d`

## Configuration

The app is configured with environments variables.
If no match is found within the env, a default value is read from a yaml file in simplenotes-app/src/main/resources/application.yaml.
