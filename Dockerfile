FROM openjdk:15-alpine as jdkbuilder

RUN apk add --no-cache binutils

ENV MODULES java.base,java.compiler,java.desktop,java.instrument,java.logging,java.management,java.naming,java.scripting,java.security.jgss,java.sql,java.sql.rowset,java.transaction.xa,java.xml,jdk.net

RUN jlink --output /myjdk --module-path $JAVA_HOME/jmods --add-modules $MODULES --no-header-files --no-man-pages --strip-debug --compress=2

RUN strip -p --strip-unneeded /myjdk/lib/server/libjvm.so

FROM alpine

RUN apk add --no-cache curl

RUN mkdir /app
RUN mkdir /app/data

COPY --from=jdkbuilder /myjdk /myjdk
COPY app/build/libs/app-with-dependencies*.jar /app/simplenotes.jar
WORKDIR /app

VOLUME /app/data

ENV SERVER_HOST 0.0.0.0

CMD [ \
    "/myjdk/bin/java", \
    "--add-opens", \
    "java.base/java.nio=ALL-UNNAMED", \
    "-server", \
    "-XX:+UnlockExperimentalVMOptions", \
    "-Xms64m", \
    "-Xmx256m", \
    "-XX:+UseG1GC", \
    "-XX:MaxGCPauseMillis=100", \
    "-XX:+UseStringDeduplication", \
    "-jar", \
    "simplenotes.jar" \
]
