package be.simplenotes.persistence

import be.simplenotes.persistence.repositories.UserRepository
import be.simplenotes.persistence.users.fakeUser
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.ktorm.database.Database
import org.ktorm.dsl.eq
import org.ktorm.entity.find
import org.ktorm.entity.toList

internal class UserRepositoryImplTest : DbTest() {

    private lateinit var userRepo: UserRepository
    private lateinit var db: Database

    @BeforeEach
    fun setup() {
        userRepo = beanContext.getBean()
        db = beanContext.getBean()
    }

    @Test
    fun `insert user`() {
        val user = fakeUser()
        assertThat(userRepo.create(user))
            .isNotNull
            .extracting("username", "password")
            .contains(user.username, user.password)

        assertThat(db.users.find { Users.username eq user.username }).isNotNull
        assertThat(db.users.toList()).hasSize(1)
        assertThat(userRepo.create(user)).isNull()
    }

    @Nested
    inner class Query {

        @Test
        fun `query existing user`() {
            val user = fakeUser()
            userRepo.create(user)

            val foundUserMaybe = userRepo.find(user.username)
            assertThat(foundUserMaybe).isNotNull
            val foundUser = foundUserMaybe!!
            assertThat(foundUser)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(user)
            assertThat(userRepo.exists(user.username)).isTrue
            assertThat(userRepo.find(foundUser.id)).isEqualTo(foundUser)
            assertThat(userRepo.exists(foundUser.id)).isTrue
        }

        @Test
        fun `query non existing user`() {
            assertThat(userRepo.find("I don't exist")).isNull()
            assertThat(userRepo.find(1)).isNull()
            assertThat(userRepo.exists(1)).isFalse
            assertThat(userRepo.exists("I don't exist")).isFalse
        }
    }

    @Nested
    inner class Delete {

        @Test
        fun `delete existing user`() {
            val user = fakeUser()
            userRepo.create(user)

            val foundUser = userRepo.find(user.username)!!
            assertThat(userRepo.delete(foundUser.id)).isTrue
            assertThat(db.users.toList()).isEmpty()
        }

        @Test
        fun `delete non existing user`() {
            assertThat(userRepo.delete(1)).isFalse
        }
    }
}
