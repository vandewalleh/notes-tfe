package be.simplenotes.persistence

import io.micronaut.context.ApplicationContext
import io.micronaut.context.BeanContext
import org.flywaydb.core.Flyway
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.parallel.ResourceLock
import javax.sql.DataSource

@ResourceLock("h2")
abstract class DbTest {

    val beanContext = ApplicationContext.build().deduceEnvironment(false).environments("test").start()

    inline fun <reified T> BeanContext.getBean(): T = getBean(T::class.java)

    @BeforeEach
    fun beforeEach() {
        val migration = beanContext.getBean<DbMigrations>()
        val dataSource = beanContext.getBean<DataSource>()

        Flyway.configure()
            .dataSource(dataSource)
            .load()
            .clean()

        migration.migrate()
    }

    @AfterAll
    fun closeCtx() {
        beanContext.close()
    }
}
