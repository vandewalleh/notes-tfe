CREATE VIEW NotesWithTags AS
     SELECT n.*, coalesce(ARRAY_AGG(t.NAME), ARRAY[]) as tags
       FROM Notes n
  LEFT JOIN Tags t ON n.uuid = t.note_uuid
   GROUP BY n.uuid
