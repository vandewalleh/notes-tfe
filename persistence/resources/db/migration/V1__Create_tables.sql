create table Users
(
    id       int auto_increment primary key,
    username varchar(50)  not null,
    password varchar(255) not null,

    constraint username unique (username)
);

create table Notes
(
    uuid       uuid        not null primary key,
    title      varchar(50) not null,
    markdown   mediumtext  not null,
    html       mediumtext  not null,
    user_id    int         not null,
    updated_at datetime    null,

    constraint Notes_fk_user foreign key (user_id) references Users (id) on delete cascade
);

create index user_id on Notes (user_id);

create table Tags
(
    id        int auto_increment primary key,
    name      varchar(50) not null,
    note_uuid uuid        not null,
    constraint Tags_fk_note foreign key (note_uuid) references Notes (uuid) on delete cascade
);

create index note_uuid on Tags (note_uuid);
