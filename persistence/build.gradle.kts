import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.micronaut")
    kotlin("kapt")
    `java-test-fixtures`
}

dependencies {
    implementation(project(":types"))
    implementation(project(":config"))

    implementation(libs.bundles.database)

    implementation(libs.slf4j.api)
    runtimeOnly(libs.slf4j.logback)

    compileOnly(libs.mapstruct.core)
    kapt(libs.mapstruct.processor)

    testImplementation(libs.bundles.test)
    testCompileOnly(libs.slf4j.logback)

    testFixturesImplementation(project(":types"))
    testFixturesImplementation(project(":config"))
    testFixturesImplementation(project(":persistence"))

    testFixturesImplementation(libs.faker) {
        exclude(group = "org.yaml")
    }

    testFixturesImplementation(libs.yaml)

    testFixturesImplementation(libs.bundles.database)
    testFixturesImplementation(libs.junit.jupiter)

    micronaut()

    testFixturesImplementation(kotlin("bom"))
}

kotlin.sourceSets["testFixtures"].kotlin.srcDirs("testfixtures")
