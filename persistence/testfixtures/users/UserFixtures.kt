package be.simplenotes.persistence.users

import be.simplenotes.persistence.repositories.UserRepository
import be.simplenotes.types.PersistedUser
import be.simplenotes.types.User
import com.github.javafaker.Faker

private val faker = Faker()

fun fakeUser() = User(faker.name().username(), faker.internet().password())

fun UserRepository.createFakeUser(): PersistedUser? {
    return create(fakeUser())
}
