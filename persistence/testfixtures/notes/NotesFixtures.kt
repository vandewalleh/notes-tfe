package be.simplenotes.persistence.notes

import be.simplenotes.persistence.repositories.NoteRepository
import be.simplenotes.types.*
import com.github.javafaker.Faker
import java.util.*

private val faker = Faker()

fun fakeUuid() = UUID.randomUUID()!!
fun fakeTitle() = faker.lorem().characters(3, 50)!!
fun tagGenerator() = generateSequence { faker.lorem().word() }
fun fakeTags() = tagGenerator().take(faker.random().nextInt(0, 3)).toList()
fun fakeContent() = faker.lorem().paragraph(faker.random().nextInt(0, 3))!!
fun fakeNote() = Note(fakeTitle(), fakeTags(), fakeContent(), fakeContent())

fun PersistedNote.toPersistedMeta() =
    PersistedNoteMetadata(title, tags, updatedAt, uuid)

fun NoteRepository.insertFakeNote(
    userId: Int,
    title: String,
    tags: List<String> = emptyList(),
    md: String = "md",
    html: String = "html",
): PersistedNote = create(userId, Note(title, tags, md, html))

fun NoteRepository.insertFakeNote(
    user: PersistedUser,
    title: String = fakeTitle(),
    tags: List<String> = fakeTags(),
    md: String = fakeContent(),
    html: String = fakeContent(),
): PersistedNote = insertFakeNote(user.id, title, tags, md, html)

fun NoteRepository.insertFakeNotes(user: PersistedUser, count: Int): List<PersistedNote> =
    generateSequence { insertFakeNote(user) }.take(count).toList()
