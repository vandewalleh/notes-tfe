package be.simplenotes.persistence.repositories

import be.simplenotes.types.PersistedUser
import be.simplenotes.types.User

interface UserRepository {
    fun create(user: User): PersistedUser?
    fun find(username: String): PersistedUser?
    fun find(id: Int): PersistedUser?
    fun exists(username: String): Boolean
    fun exists(id: Int): Boolean
    fun delete(id: Int): Boolean
    fun findAll(): List<Int>
}
