package be.simplenotes.persistence.repositories

import be.simplenotes.persistence.Users
import be.simplenotes.persistence.converters.UserConverter
import be.simplenotes.persistence.users
import be.simplenotes.types.PersistedUser
import be.simplenotes.types.User
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.any
import org.ktorm.entity.find
import java.sql.SQLIntegrityConstraintViolationException
import javax.inject.Singleton

@Singleton
internal class UserRepositoryImpl(
    private val db: Database,
    private val converter: UserConverter,
) : UserRepository {
    override fun create(user: User) = try {
        val id = db.insertAndGenerateKey(Users) {
            set(it.username, user.username)
            set(it.password, user.password)
        } as Int
        PersistedUser(user.username, user.password, id)
    } catch (e: SQLIntegrityConstraintViolationException) {
        null
    }

    override fun find(username: String) = db.users.find { it.username eq username }
        .let { converter.toPersistedUser(it) }

    override fun find(id: Int) = db.users.find { it.id eq id }
        .let { converter.toPersistedUser(it) }

    override fun exists(username: String) = db.users.any { it.username eq username }
    override fun exists(id: Int) = db.users.any { it.id eq id }
    override fun delete(id: Int) = db.delete(Users) { it.id eq id } == 1
    override fun findAll() = db.from(Users).select(Users.id).map { it.getInt(1) }
}
