package be.simplenotes.persistence.repositories

import be.simplenotes.types.ExportedNote
import be.simplenotes.types.Note
import be.simplenotes.types.PersistedNote
import be.simplenotes.types.PersistedNoteMetadata
import java.util.*

interface NoteRepository {

    fun findAll(
        userId: Int,
        limit: Int = 20,
        offset: Int = 0,
        tag: String? = null,
        deleted: Boolean = false
    ): List<PersistedNoteMetadata>

    fun count(userId: Int, tag: String? = null, deleted: Boolean = false): Int
    fun delete(userId: Int, uuid: UUID, permanent: Boolean = false): Boolean
    fun restore(userId: Int, uuid: UUID): Boolean

    // These methods only access notes where `Notes.deleted = false`
    fun getTags(userId: Int): List<String>
    fun exists(userId: Int, uuid: UUID): Boolean
    fun create(userId: Int, note: Note): PersistedNote
    fun find(userId: Int, uuid: UUID): PersistedNote?
    fun update(userId: Int, uuid: UUID, note: Note): PersistedNote?
    fun export(userId: Int): List<ExportedNote>
    fun findAllDetails(userId: Int): List<PersistedNote>

    fun makePublic(userId: Int, uuid: UUID): Boolean
    fun makePrivate(userId: Int, uuid: UUID): Boolean
    fun findPublic(uuid: UUID): PersistedNote?
}
