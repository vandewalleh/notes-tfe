package be.simplenotes.persistence

import be.simplenotes.persistence.extensions.varcharArray
import org.ktorm.schema.*

internal object Users : Table<UserEntity>("Users") {
    val id = int("id").primaryKey().bindTo { it.id }
    val username = varchar("username").bindTo { it.username }
    val password = varchar("password").bindTo { it.password }
}

internal object Notes : Table<NoteEntity>("Notes") {
    val uuid = uuid("uuid").primaryKey().bindTo { it.uuid }
    val title = varchar("title").bindTo { it.title }
    val markdown = text("markdown").bindTo { it.markdown }
    val html = text("html").bindTo { it.html }
    val userId = int("user_id").references(Users) { it.user }
    val updatedAt = datetime("updated_at").bindTo { it.updatedAt }
    val deleted = boolean("deleted").bindTo { it.deleted }
    val public = boolean("public").bindTo { it.public }
    val user get() = userId.referenceTable as Users
}

internal object Tags : Table<TagEntity>("Tags") {
    val id = int("id").primaryKey().bindTo { it.id }
    val name = varchar("name").bindTo { it.name }
    val noteUuid = uuid("note_uuid").references(Notes) { it.note }
    val note: Notes get() = noteUuid.referenceTable as Notes
}

internal object NotesWithTags : Table<NoteWithTagsEntity>("NotesWithTags") {
    val uuid = uuid("uuid").primaryKey().bindTo { it.uuid }
    val title = varchar("title").bindTo { it.title }
    val markdown = text("markdown").bindTo { it.markdown }
    val html = text("html").bindTo { it.html }
    val userId = int("user_id").references(Users) { it.user }
    val updatedAt = datetime("updated_at").bindTo { it.updatedAt }
    val deleted = boolean("deleted").bindTo { it.deleted }
    val public = boolean("public").bindTo { it.public }
    val tags = varcharArray("tags").bindTo { it.tags }
    val user get() = userId.referenceTable as Users
}
