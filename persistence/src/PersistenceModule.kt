package be.simplenotes.persistence

import be.simplenotes.config.DataSourceConfig
import be.simplenotes.persistence.extensions.CustomSqlFormatter
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import org.ktorm.database.Database
import org.ktorm.database.SqlDialect
import javax.inject.Singleton
import javax.sql.DataSource

@Factory
class PersistenceModule {

    @Singleton
    internal fun database(migrations: DbMigrations, dataSource: DataSource): Database {
        migrations.migrate()
        return Database.connect(dataSource, dialect = object : SqlDialect {
            override fun createSqlFormatter(database: Database, beautifySql: Boolean, indentSize: Int) =
                CustomSqlFormatter(database, beautifySql, indentSize)
        })
    }

    @Singleton
    @Bean(preDestroy = "close")
    internal fun dataSource(conf: DataSourceConfig): HikariDataSource {
        val hikariConfig = HikariConfig().also {
            it.jdbcUrl = conf.jdbcUrl
            it.driverClassName = "org.h2.Driver"
            it.username = ""
            it.password = ""
            it.maximumPoolSize = conf.maximumPoolSize
            it.connectionTimeout = conf.connectionTimeout
            it.dataSourceProperties["CASE_INSENSITIVE_IDENTIFIERS"] = "TRUE"
        }
        return HikariDataSource(hikariConfig)
    }
}
