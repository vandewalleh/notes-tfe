package be.simplenotes.persistence.converters

import be.simplenotes.persistence.NoteEntity
import be.simplenotes.persistence.NoteWithTagsEntity
import be.simplenotes.persistence.UserEntity
import be.simplenotes.types.*
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ReportingPolicy
import java.time.LocalDateTime
import java.util.*

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "jsr330")
internal interface UserConverter {
    fun toPersistedUser(userEntity: UserEntity?): PersistedUser?
}

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "jsr330")
internal abstract class NoteConverter {

    @Mapping(target = "trash", source = "deleted")
    abstract fun toExportedNote(entity: NoteWithTagsEntity?): ExportedNote?
    abstract fun toPersistedNote(entity: NoteWithTagsEntity?): PersistedNote?
    abstract fun toPersistedNoteMetadata(entity: NoteWithTagsEntity?): PersistedNoteMetadata?

    fun toEntity(note: Note, uuid: UUID, userId: Int, updatedAt: LocalDateTime) = NoteEntity {
        this.title = note.title
        this.markdown = note.markdown
        this.html = note.html
        this.uuid = uuid
        this.user.id = userId
        this.updatedAt = updatedAt
    }
}
