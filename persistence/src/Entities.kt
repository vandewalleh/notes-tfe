package be.simplenotes.persistence

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import java.time.LocalDateTime
import java.util.*

internal val Database.users get() = sequenceOf(Users, withReferences = false)
internal val Database.notes get() = sequenceOf(Notes, withReferences = false)
internal val Database.tags get() = sequenceOf(Tags, withReferences = false)
internal val Database.noteWithTags get() = sequenceOf(NotesWithTags, withReferences = false)

internal interface UserEntity : Entity<UserEntity> {
    companion object : Entity.Factory<UserEntity>()

    var id: Int
    var username: String
    var password: String
}

internal interface NoteEntity : Entity<NoteEntity> {
    companion object : Entity.Factory<NoteEntity>()

    var uuid: UUID
    var title: String
    var markdown: String
    var html: String
    var updatedAt: LocalDateTime
    var deleted: Boolean
    var public: Boolean
    var user: UserEntity
}

internal interface TagEntity : Entity<TagEntity> {
    companion object : Entity.Factory<TagEntity>()

    val id: Int
    var name: String
    var note: NoteEntity
}

internal interface NoteWithTagsEntity : Entity<NoteWithTagsEntity> {
    companion object : Entity.Factory<NoteWithTagsEntity>()

    var uuid: UUID
    var title: String
    var markdown: String
    var html: String
    var updatedAt: LocalDateTime
    var deleted: Boolean
    var public: Boolean
    var user: UserEntity
    val tags: List<String>
}
