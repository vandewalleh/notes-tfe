package be.simplenotes.persistence

import org.flywaydb.core.Flyway
import javax.inject.Singleton
import javax.sql.DataSource

interface DbMigrations {
    fun migrate()
}

@Singleton
internal class DbMigrationsImpl(private val dataSource: DataSource) : DbMigrations {
    override fun migrate() {
        Flyway.configure()
            .dataSource(dataSource)
            .locations("db/migration")
            .load()
            .migrate()
    }
}
