plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.kotlinx-serialization")
}

dependencies {
    implementation(libs.kotlinx.serialization.json)
}
