@file:UseContextualSerialization(LocalDateTime::class, UUID::class)

package be.simplenotes.types

import kotlinx.serialization.Serializable
import kotlinx.serialization.UseContextualSerialization
import java.time.LocalDateTime
import java.util.*

@Serializable
data class NoteMetadata(
    val title: String,
    val tags: List<String>,
)

@Serializable
data class PersistedNoteMetadata(
    val title: String,
    val tags: List<String>,
    val updatedAt: LocalDateTime,
    val uuid: UUID,
)

@Serializable
data class Note(
    val title: String,
    val tags: List<String>,
    val markdown: String,
    val html: String,
)

@Serializable
data class PersistedNote(
    val uuid: UUID,
    val title: String,
    val tags: List<String>,
    val markdown: String,
    val html: String,
    val updatedAt: LocalDateTime,
    val public: Boolean,
)

@Serializable
data class ExportedNote(
    val title: String,
    val tags: List<String>,
    val markdown: String,
    val html: String,
    val updatedAt: LocalDateTime,
    val trash: Boolean,
)
