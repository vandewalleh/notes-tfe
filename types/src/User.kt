package be.simplenotes.types

data class User(val username: String, val password: String)
data class PersistedUser(val username: String, val password: String, val id: Int)
data class LoggedInUser(val userId: Int, val username: String) {
    constructor(user: PersistedUser) : this(user.id, user.username)
}
