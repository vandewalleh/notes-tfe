package be.simplenotes.views

import be.simplenotes.types.LoggedInUser
import be.simplenotes.views.components.Alert
import be.simplenotes.views.components.alert
import be.simplenotes.views.components.input
import be.simplenotes.views.components.submitButton
import io.konform.validation.ValidationError
import kotlinx.html.*
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class UserView(@Named("styles") styles: String) : View(styles) {
    fun register(
        loggedInUser: LoggedInUser?,
        error: String? = null,
        validationErrors: List<ValidationError> = emptyList(),
    ) = accountForm(
        "Register",
        "Registration page",
        loggedInUser,
        error,
        validationErrors,
        "Create an account",
        "Register"
    ) {
        +"Already have an account? "
        a(href = "/login", classes = "no-underline text-blue-500 hover:text-blue-400 font-bold") { +"Sign In" }
    }

    fun login(
        loggedInUser: LoggedInUser?,
        error: String? = null,
        validationErrors: List<ValidationError> = emptyList(),
        new: Boolean = false,
    ) = accountForm("Login", "Login page", loggedInUser, error, validationErrors, "Sign In", "Sign In", new) {
        +"Don't have an account yet? "
        a(href = "/register", classes = "no-underline text-blue-500 hover:text-blue-400 font-bold") {
            +"Create an account"
        }
    }

    private fun accountForm(
        title: String,
        description: String,
        loggedInUser: LoggedInUser?,
        error: String? = null,
        validationErrors: List<ValidationError> = emptyList(),
        h1: String,
        submit: String,
        new: Boolean = false,
        footer: FlowContent.() -> Unit,
    ) = renderPage(title = title, description, loggedInUser = loggedInUser) {
        div("centered container mx-auto flex justify-center items-center") {
            div("w-full md:w-1/2 lg:w-1/3 m-4") {
                div("p-8 mb-6") {
                    h1("font-semibold text-lg mb-6 text-center") { +h1 }
                    if (new) alert(Alert.Success, "Your account has been created")
                    error?.let { alert(Alert.Warning, error) }
                    form(method = FormMethod.post) {
                        input(
                            id = "username",
                            placeholder = "Username",
                            autoComplete = "username",
                            error = validationErrors.find { it.dataPath == ".username" }?.message
                        )
                        input(
                            id = "password",
                            placeholder = "Password",
                            autoComplete = "new-password",
                            type = InputType.password,
                            error = validationErrors.find { it.dataPath == ".password" }?.message
                        )
                        submitButton(submit)
                    }
                }
                div("text-center") {
                    p("text-gray-200 text-sm") {
                        footer()
                    }
                }
            }
        }
    }
}
