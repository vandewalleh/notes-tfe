package be.simplenotes.views

import be.simplenotes.types.LoggedInUser
import kotlinx.html.*
import kotlinx.html.ThScope.col
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class BaseView(@Named("styles") styles: String) : View(styles) {
    fun renderHome(loggedInUser: LoggedInUser?) = renderPage(
        title = "Home",
        description = "A fast and simple note taking website",
        loggedInUser = loggedInUser
    ) {
        section("text-center my-2 p-2") {
            h1("text-5xl casual") {
                span("text-teal-300") { +"SimpleNotes " }
                +"- access your notes anywhere"
            }
        }

        div("container mx-auto flex flex-wrap justify-center content-center") {

            div("md:order-1 order-2 flipped p-4 my-10 w-full md:w-1/2") {
                attributes["aria-label"] = "demo"
                div("flex justify-between mb-4") {
                    h1("text-2xl underline") { +"Notes" }
                    span {
                        span("btn btn-teal pointer-events-none") { +"Trash (3)" }
                        span("ml-2 btn btn-green pointer-events-none") { +"New" }
                    }
                }
                form(classes = "md:space-x-2") {
                    id = "search"
                    input {
                        attributes["aria-label"] = "demo-search"
                        attributes["name"] = "search"
                        attributes["disabled"] = ""
                        attributes["value"] = "tag:demo"
                    }
                    span {
                        id = "buttons"
                        button(type = ButtonType.button, classes = "btn btn-green pointer-events-none") {
                            attributes["disabled"] = ""
                            +"search"
                        }
                        span("btn btn-red pointer-events-none") { +"clear" }
                    }
                }
                div("overflow-x-auto") {
                    demoTable()
                }
            }
            welcome()
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun DIV.demoTable() {
        table {
            id = "notes"
            thead {
                tr {
                    th(scope = col, classes = "w-1/2") { +"Title" }
                    th(scope = col, classes = "w-1/4") { +"Updated" }
                    th(scope = col, classes = "w-1/4") { +"Tags" }
                }
            }
            tbody {
                listOf(
                    Triple("Formula 1", "moments ago", arrayOf("#demo")),
                    Triple("Syntax highlighting", "2 hours ago", arrayOf("#features", "#demo")),
                    Triple("report", "5 days ago", arrayOf("#study", "#demo")),
                ).forEach { (title, ago, tags) ->
                    tr {
                        td { span("text-blue-200 font-semibold underline") { +title } }
                        td("text-center") { +ago }
                        td {
                            ul("inline flex flex-wrap justify-center") {
                                tags.forEach { tag ->
                                    li("mx-2 my-1") { span("tag disabled") { +tag } }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun DIV.welcome() {
        div("w-full my-auto md:w-1/2 md:order-2 order-1 text-center") {
            div("m-4 rounded-lg p-6") {
                h2("text-3xl text-teal-400 underline") { +"Features:" }
                ul("list-disc text-lg list-inside") {
                    li { +"Markdown support" }
                    li { +"Full text search" }
                    li { +"Structured search" }
                    li { +"Code highlighting" }
                    li { +"Fast and lightweight" }
                    li { +"No tracking" }
                    li { +"Works without javascript" }
                    li { +"Data export" }
                }
            }
        }
    }
}
