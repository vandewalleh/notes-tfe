package be.simplenotes.views

import be.simplenotes.types.LoggedInUser
import be.simplenotes.views.components.navbar
import kotlinx.html.*
import kotlinx.html.stream.appendHTML

abstract class View(private val styles: String) {

    fun renderPage(
        title: String,
        description: String? = null,
        loggedInUser: LoggedInUser?,
        scripts: List<String> = emptyList(),
        body: MAIN.() -> Unit = {},
    ) = buildString {
        appendLine("<!DOCTYPE html>")
        appendHTML().html {
            attributes["lang"] = "en"
            head {
                meta(charset = "UTF-8")
                meta(name = "viewport", content = "width=device-width, initial-scale=1")
                title("$title - SimpleNotes")
                description?.let { meta(name = "description", content = it) }
                link(rel = "preload", href = "/recursive-0.0.1.woff2") {
                    attributes["as"] = "font"
                    attributes["type"] = "font/woff2"
                    attributes["crossorigin"] = "anonymous"
                }
                link(rel = "stylesheet", href = styles)
                icons()
                scripts.forEach { src ->
                    script(src = src) {}
                }
            }
            body("bg-gray-900 text-white") {
                navbar(loggedInUser)
                main { body() }
            }
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun HEAD.icons() {
        link(rel = "apple-touch-icon", href = "/apple-touch-icon.png") { attributes["sizes"] = "180x180" }
        link(rel = "icon", href = "/favicon-32x32.png", type = "image/png") { attributes["sizes"] = "32x32" }
        link(rel = "icon", href = "/favicon-16x16.png", type = "image/png") { attributes["sizes"] = "16x16" }
        link(rel = "manifest", href = "/site.webmanifest")
        link(rel = "mask-icon", href = "/safari-pinned-tab.svg") { attributes["color"] = "#2c7a7b" }
        meta(name = "msapplication-TileColor", content = "#00aba9")
        meta(name = "theme-color", content = "#2c7a7b")
    }
}
