package be.simplenotes.views

import be.simplenotes.types.LoggedInUser
import be.simplenotes.views.components.Alert
import be.simplenotes.views.components.alert
import be.simplenotes.views.components.input
import be.simplenotes.views.extensions.summary
import io.konform.validation.ValidationError
import kotlinx.html.*
import kotlinx.html.ButtonType.submit
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class SettingView(@Named("styles") styles: String) : View(styles) {

    fun settings(
        loggedInUser: LoggedInUser,
        error: String? = null,
        validationErrors: List<ValidationError> = emptyList(),
    ) = renderPage("Settings", loggedInUser = loggedInUser) {
        div("container mx-auto") {

            section("m-4 p-4 bg-gray-800 rounded") {
                h1("text-xl") {
                    +"Welcome "
                    span("text-teal-200 font-semibold") { +loggedInUser.username }
                }
            }

            section("m-4 p-2 bg-gray-800 rounded flex flex-wrap justify-around items-end") {

                form(classes = "m-2", method = FormMethod.post, action = "/export") {
                    button(
                        name = "display",
                        classes = "inline btn btn-teal block",
                        type = submit
                    ) { +"Display my data" }
                }

                form(classes = "m-2", method = FormMethod.post, action = "/export") {

                    div {
                        listOf("json", "zip").forEach { format ->
                            radioInput(name = "format") {
                                id = format
                                attributes["value"] = format
                                if (format == "json") attributes["checked"] = ""
                                else attributes["class"] = "ml-4"
                            }
                            label(classes = "ml-2") {
                                attributes["for"] = format
                                +format
                            }
                        }
                    }

                    button(name = "download", classes = "inline btn btn-green block mt-2", type = submit) {
                        +"Download my data"
                    }
                }
            }

            section(classes = "m-4 p-4 bg-gray-800 rounded") {
                h2(classes = "mb-4 text-red-400 text-lg font-semibold") {
                    +"Delete my account"
                }

                error?.let { alert(Alert.Warning, error) }

                details {

                    if (error != null || validationErrors.isNotEmpty()) {
                        attributes["open"] = ""
                    }

                    summary {
                        span(classes = "mb-4 font-semibold underline") {
                            +"Are you sure? "
                            +"You are about to delete this user, and this process is irreversible !"
                        }
                    }

                    form(classes = "mt-4", method = FormMethod.post) {
                        input(
                            id = "password",
                            placeholder = "Password",
                            autoComplete = "off",
                            type = InputType.password,
                            error = validationErrors.find { it.dataPath == ".password" }?.message
                        )
                        checkBoxInput(name = "checked") {
                            id = "checked"
                            attributes["required"] = ""
                            label {
                                attributes["for"] = "checked"
                                +" Do you want to proceed ?"
                            }
                        }
                        button(
                            type = submit,
                            classes = "block mt-4 btn btn-red",
                            name = "delete"
                        ) { +"I'm sure" }
                    }
                }
            }
        }
    }
}
