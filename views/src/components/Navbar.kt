package be.simplenotes.views.components

import be.simplenotes.types.LoggedInUser
import kotlinx.html.*

internal fun BODY.navbar(loggedInUser: LoggedInUser?) {
    nav {
        id = "navbar"
        a("/") {
            id = "home"
            +"SimpleNotes"
        }
        ul("space-x-2") {
            id = "navigation"
            if (loggedInUser != null) {
                val links = listOf(
                    "/notes" to "Notes",
                    "/settings" to "Settings",
                )
                links.forEach { (href, name) ->
                    li("txt") {
                        a(href = href) { +name }
                    }
                }
                li {
                    form(action = "/logout", method = FormMethod.post) {
                        button(type = ButtonType.submit, classes = "btn btn-green") { +"Logout" }
                    }
                }
            } else {
                li {
                    a(href = "/login", classes = "btn btn-green") { +"Sign In" }
                }
            }
        }
    }
}
