package be.simplenotes.views.components

import kotlinx.html.*
import kotlinx.html.ButtonType.submit
import kotlinx.html.FormMethod.post

internal fun DIV.noteListHeader(numberOfDeletedNotes: Int, query: String = "") {
    div("flex justify-between mb-4") {
        h1("text-2xl underline") { +"Notes" }
        span {
            a(
                href = "/notes/trash",
                classes = "btn btn-teal"
            ) { +"Trash ($numberOfDeletedNotes)" }
            a(
                href = "/notes/new",
                classes = "ml-2 btn btn-green"
            ) { +"New" }
        }
    }
    form(method = post, classes = "md:space-x-2") {
        id = "search"
        input(name = "search") {
            attributes["value"] = query
            attributes["aria-label"] = "search"
        }
        span {
            id = "buttons"
            button(type = submit, classes = "btn btn-green") {
                +"search"
            }
            a(href = "/notes", classes = "btn btn-red") {
                +"clear"
            }
        }
    }
}
