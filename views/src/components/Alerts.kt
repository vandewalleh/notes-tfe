package be.simplenotes.views.components

import kotlinx.html.*

internal fun FlowContent.alert(type: Alert, title: String, details: String? = null, multiline: Boolean = false) {
    val colors = when (type) {
        Alert.Success -> "bg-green-500 border border-green-400 text-gray-800"
        Alert.Warning -> "bg-red-500 border border-red-400 text-red-200"
    }
    div("$colors px-4 py-3 mb-4 rounded relative") {
        attributes["role"] = "alert"
        strong("font-bold") { +title }
        details?.let {
            if (multiline) p { +details }
            else span("block sm:inline") { +details }
        }
    }
}

internal enum class Alert {
    Success, Warning
}
