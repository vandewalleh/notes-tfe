package be.simplenotes.views.components

import be.simplenotes.types.PersistedNoteMetadata
import be.simplenotes.views.utils.toTimeAgo
import kotlinx.html.*
import kotlinx.html.ButtonType.submit
import kotlinx.html.FormMethod.post
import kotlinx.html.ThScope.col

internal fun FlowContent.deletedNoteTable(notes: List<PersistedNoteMetadata>) = div("overflow-x-auto") {
    table {
        id = "notes"
        thead {
            tr {
                th(col, "w-1/4") { +"Title" }
                th(col, "w-1/4") { +"Updated" }
                th(col, "w-1/4") { +"Tags" }
                th(col, "w-1/4") { +"Restore" }
            }
        }
        tbody {
            notes.forEach { (title, tags, updatedAt, uuid) ->
                tr {
                    td { +title }
                    td("text-center") { +updatedAt.toTimeAgo() }
                    td { tags(tags) }
                    td("text-center") {
                        form(method = post, action = "/notes/deleted/$uuid") {
                            button(classes = "btn btn-red mb-2", type = submit, name = "delete") {
                                +"Delete permanently"
                            }
                            button(classes = "ml-2 btn btn-green", type = submit, name = "restore") {
                                +"Restore"
                            }
                        }
                    }
                }
            }
        }
    }
}

private fun FlowContent.tags(tags: List<String>) {
    ul("inline flex flex-wrap justify-center") {
        tags.forEach { tag ->
            li("mx-2 my-1") {
                span("tag disabled") { +"#$tag" }
            }
        }
    }
}
