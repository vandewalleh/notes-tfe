package be.simplenotes.views.components

import be.simplenotes.types.PersistedNoteMetadata
import be.simplenotes.views.utils.toTimeAgo
import kotlinx.html.*
import kotlinx.html.ThScope.col

internal fun FlowContent.noteTable(notes: List<PersistedNoteMetadata>) = div("overflow-x-auto") {
    table {
        id = "notes"
        thead {
            tr {
                th(col, "w-1/2") { +"Title" }
                th(col, "w-1/4") { +"Updated" }
                th(col, "w-1/4") { +"Tags" }
            }
        }
        tbody {
            notes.forEach { (title, tags, updatedAt, uuid) ->
                tr {
                    td {
                        a(classes = "text-blue-200  font-semibold underline", href = "/notes/$uuid") { +title }
                    }
                    td("text-center") {
                        +updatedAt.toTimeAgo()
                    }
                    td { tags(tags) }
                }
            }
        }
    }
}

private fun FlowContent.tags(tags: List<String>) {
    ul("inline flex flex-wrap justify-center") {
        tags.forEach { tag ->
            li("mx-2 my-1") {
                a(href = "?tag=$tag", classes = "tag") { +"#$tag" }
            }
        }
    }
}
