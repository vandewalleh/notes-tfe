package be.simplenotes.views.components

import kotlinx.html.*
import kotlinx.html.ButtonType.submit

internal fun FlowContent.input(
    type: InputType = InputType.text,
    placeholder: String,
    id: String,
    autoComplete: String? = null,
    error: String? = null
) {
    val colors = "bg-gray-800 border-gray-700 focus:border-teal-500 text-white"
    div("mb-8") {
        input(
            type = type,
            classes = "$colors rounded w-full border appearance-none focus:outline-none text-base p-2"
        ) {
            attributes["placeholder"] = placeholder
            attributes["aria-label"] = placeholder
            attributes["name"] = id
            attributes["id"] = id
            autoComplete?.let { attributes["autocomplete"] = it }
        }
        error?.let { p("mt-2 text-red-500 text-sm italic") { +"$placeholder $error" } }
    }
}

internal fun FlowContent.submitButton(text: String) {
    div("flex items-center mt-6") {
        button(
            type = submit,
            classes = "btn btn-teal w-full"
        ) { +text }
    }
}
