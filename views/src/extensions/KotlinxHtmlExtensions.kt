package be.simplenotes.views.extensions

import kotlinx.html.*

internal class SUMMARY(consumer: TagConsumer<*>) :
    HTMLTag(
        "summary",
        consumer,
        emptyMap(),
        inlineTag = true,
        emptyTag = false
    ),
    HtmlInlineTag

internal fun DETAILS.summary(block: SUMMARY.() -> Unit = {}) {
    SUMMARY(consumer).visit(block)
}
