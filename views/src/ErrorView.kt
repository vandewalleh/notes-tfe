package be.simplenotes.views

import be.simplenotes.views.components.Alert
import be.simplenotes.views.components.alert
import kotlinx.html.a
import kotlinx.html.div
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class ErrorView(@Named("styles") styles: String) : View(styles) {

    enum class Type(val title: String) {
        SqlTransientError("Database unavailable"),
        NotFound("Not Found"),
        Other("Error"),
    }

    fun error(errorType: Type) = renderPage(errorType.title, loggedInUser = null) {
        div("container mx-auto p-4") {
            when (errorType) {
                Type.SqlTransientError -> alert(
                    Alert.Warning,
                    errorType.title,
                    "Please try again later",
                    multiline = true
                )
                Type.NotFound -> alert(Alert.Warning, errorType.title, "Page not found", multiline = true)
                Type.Other -> alert(Alert.Warning, errorType.title)
            }
            div {
                a(href = "/", classes = "btn btn-green") { +"Go back to the homepage" }
            }
        }
    }
}
