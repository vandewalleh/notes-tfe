package be.simplenotes.views.utils

import org.ocpsoft.prettytime.PrettyTime
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

private val prettyTime = PrettyTime()

internal fun LocalDateTime.toTimeAgo(): String = prettyTime.format(
    Date.from(atZone(ZoneId.systemDefault()).toInstant())
)
