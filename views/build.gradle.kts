import be.simplenotes.micronaut

plugins {
    id("be.simplenotes.base")
    id("be.simplenotes.micronaut")
}

dependencies {
    implementation(project(":types"))

    micronaut()

    implementation(libs.konform)
    implementation(libs.kotlinx.html)
    implementation(libs.prettytime)
}
