@file:Suppress("SpellCheckingInspection")

package be.simplenotes

object Libs {

    object Micronaut {
        private const val version = "2.5.1"
        const val inject = "io.micronaut:micronaut-inject:$version"
        const val processor = "io.micronaut:micronaut-inject-java:$version"
    }

}
