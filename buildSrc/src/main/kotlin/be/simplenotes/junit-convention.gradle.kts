package be.simplenotes

plugins {
    java apply false
}

tasks.withType<Test> {
    useJUnitPlatform {
        excludeTags("slow")
    }
}

dependencies {
    testRuntimeOnly(project(":junit-config"))
}
