package be.simplenotes

plugins {
    id("be.simplenotes.java-convention")
    id("be.simplenotes.kotlin-convention")
    id("be.simplenotes.junit-convention")
    id("org.jlleitschuh.gradle.ktlint")
}
