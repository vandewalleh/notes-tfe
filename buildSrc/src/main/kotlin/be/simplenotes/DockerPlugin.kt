package be.simplenotes

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.create

open class DockerPluginExtension {
    var image: String? = null
    var tag = "latest"
}

class DockerPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create<DockerPluginExtension>("docker")

        project.task("dockerBuild") {
            dependsOn("package")

            group = "docker"
            description = "Build a docker image"

            doLast {
                project.exec {
                    commandLine("docker", "build", "-t", "${extension.image}:${extension.tag}", ".")
                    workingDir(project.rootProject.projectDir)
                }
            }
        }

        project.task("dockerPush") {
            dependsOn("dockerBuild")

            group = "docker"
            description = "Push a docker image"

            doLast {
                project.exec {
                    commandLine("docker", "push", "${extension.image}:${extension.tag}")
                    workingDir(project.rootProject.projectDir)
                }
            }
        }
    }
}
