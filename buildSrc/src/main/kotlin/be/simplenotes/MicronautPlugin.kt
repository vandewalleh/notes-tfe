package be.simplenotes

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.jetbrains.kotlin.gradle.plugin.KaptExtension

class MicronautPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        target.plugins.apply("org.jetbrains.kotlin.kapt")
        target.extensions.configure<KaptExtension>("kapt") {
            arguments {
                arg("micronaut.processing.incremental", true)
            }
        }
    }
}

fun DependencyHandler.micronaut() {
    add("kapt", Libs.Micronaut.processor)
    add("implementation", Libs.Micronaut.inject)
}

fun DependencyHandler.micronautTest() {
    add("kaptTest", Libs.Micronaut.processor)
    add("testImplementation", Libs.Micronaut.inject)
}

fun DependencyHandler.micronautFixtures() {
    add("kaptTestFixtures", Libs.Micronaut.inject)
    add("testFixturesImplementation", Libs.Micronaut.processor)
}

