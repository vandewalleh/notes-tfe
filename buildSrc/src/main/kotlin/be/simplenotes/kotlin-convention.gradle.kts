package be.simplenotes

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(platform(kotlin("bom")))
    testImplementation(platform(kotlin("bom")))
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "15"
        javaParameters = true
        freeCompilerArgs = listOf(
            "-Xinline-classes",
            "-Xno-param-assertions",
            "-Xno-call-assertions",
            "-Xno-receiver-assertions"
        )
    }
}

kotlin.sourceSets["main"].kotlin.setSrcDirs(listOf("src"))
kotlin.sourceSets["test"].kotlin.setSrcDirs(listOf("test"))
