package be.simplenotes

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.register
import java.io.File

class PostcssPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        with(project.tasks) {
            register<PostcssTask>("postcss") {
                group = "postcss"
                description = "generate postcss resources"
            }
            getByName("processResources").dependsOn("postcss")
        }

        val sourceSets = project.convention.getPlugin(JavaPluginConvention::class.java).sourceSets
        val root = File("${project.buildDir}/generated-resources/css")
        sourceSets["main"].resources.srcDir(root)
    }
}
