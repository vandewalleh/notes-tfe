package be.simplenotes

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.*
import org.gradle.kotlin.dsl.getByType
import java.io.File
import java.lang.ProcessBuilder.Redirect.PIPE
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread

open class PostcssTask : DefaultTask() {

    private val viewsProject = project
        .parent
        ?.project(":views")
        ?: error("Missing :views")

    @get:InputDirectory
    val templatesDir = viewsProject.extensions
        .getByType<SourceSetContainer>()
        .asMap.getOrElse("main") { error("main sources not found") }
        .allSource.srcDirs
        .find { it.endsWith("src") }
        ?: error("kotlin sources not found")

    private val yarnRoot = File(project.rootDir, "css")

    @get:InputDirectory
    val postCssDir = File(project.rootDir, "css/src")

    @get:InputFiles
    val postCssConfig = listOf(
        "tailwind.config.js",
        "postcss.config.js",
        "package.json"
    ).map { File(yarnRoot, it) }

    @get:OutputDirectory
    val outputRootDir = File(project.buildDir, "generated-resources/css")

    private val cssIndex = File(postCssDir, "styles.pcss")

    private val cssOutput = File(outputRootDir, "static/styles.css")
    private val manifestOutput = File(outputRootDir, "css-manifest.json")

    private val purgeGlob = "$templatesDir/**/*.kt"

    @TaskAction
    fun generateCss() {
        // TODO: auto yarn install ?

        outputRootDir.deleteRecursively()

        ProcessBuilder("yarn", "run", "postcss", "$cssIndex", "--output", "$cssOutput")
            .apply {
                environment().let {
                    it["MANIFEST"] = "$manifestOutput"
                    it["NODE_ENV"] = "production"
                    it["PURGE"] = purgeGlob
                }
            }
            .redirectOutput(PIPE)
            .redirectError(PIPE)
            .directory(yarnRoot)
            .start()
            .apply {
                thread { inputStream.use { it.copyTo(System.out) } }
                thread { errorStream.use { it.copyTo(System.out) } }
                waitFor(30, TimeUnit.SECONDS)
                if (exitValue() != 0) throw GradleException(":/")
            }
    }

}
