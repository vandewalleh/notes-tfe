package be.simplenotes

import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("com.github.johnrengelman.shadow")
}

tasks.withType<ShadowJar> {

    archiveAppendix.set("with-dependencies")
    manifest.attributes["Main-Class"] = "be.simplenotes.app.SimpleNotesKt"

    mergeServiceFiles()

    File(rootProject.projectDir, "buildSrc/src/main/resources/exclusions")
        .listFiles()!!
        .flatMap {
            it.readLines()
                .asSequence()
                .map { it.trim() }
                .filterNot { it.isBlank() }
                .filterNot { it.startsWith("#") }
                .asIterable()
        }.forEach { exclude(it) }

}

tasks.create("package") {
    tasks.getByName("build").dependsOn("package")

    dependsOn("shadowJar")

    doLast {
        println("SimpleNotes Packaged !")
    }
}
