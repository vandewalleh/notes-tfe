plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.0")
    implementation("org.jetbrains.kotlin:kotlin-serialization:1.5.0")
    implementation("gradle.plugin.com.github.jengelman.gradle.plugins:shadow:7.0.0")
    implementation("org.jlleitschuh.gradle:ktlint-gradle:10.0.0")
    implementation("com.github.ben-manes:gradle-versions-plugin:0.28.0")
}
