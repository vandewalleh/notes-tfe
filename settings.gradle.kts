rootProject.name = "simplenotes"
include(":config")
include(":views")
include(":app")
include(":domain")
include(":search")
include(":types")
include(":persistence")
include(":css")
include(":junit-config")

enableFeaturePreview("VERSION_CATALOGS")
